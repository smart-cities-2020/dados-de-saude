FROM python:latest

ENV PYTHONUNBUFFERED=1

COPY . /app

RUN pip install --upgrade pip && pip install -r /app/requirements.txt

WORKDIR /app

CMD [ "python", "/app/app.py" ]
