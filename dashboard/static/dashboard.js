// Casos de Sífilis
var firstChart = echarts.init(document.getElementById('firstChart'));
var option_casos_sifilis = {
  title: {
	text: 'Casos de sífilis por ano'
  },
	color: ['#8cc1b5', '#B9D8C2', '#84C8C2'],
	tooltip: {
		trigger: 'axis',
		axisPointer: {
			type: 'cross',
			label: {
				backgroundColor: '#6a7985'
			}
		}
	},
  legend: {
	data:['Incidências'],
	bottom: '-5'
  },
  xAxis: {
	data: ["2001","2002","2003","2004","2005","2006"]
  },
  yAxis: {},
  series: [{
	name: 'Incidências',
	type: 'line',
	areaStyle: {},
	data: [500, 2000, 3600, 1000, 1050, 2300]
  }]
};

$.ajax({
  dataType: "json",
  url: "http://127.0.0.1:5000/sifilis/ano",
  success: function (data) {
	var l_ano = [];
	var l_quantidade = [];
	for (var key in data) {
	  l_ano.push(key);
	  l_quantidade.push(data[key]);
	}
	option_casos_sifilis.xAxis.data = l_ano;
	option_casos_sifilis.series[0].data = l_quantidade;
    firstChart.setOption(option_casos_sifilis);
  }
});

// Proporção de casos por sexo
var secondChart = echarts.init(document.getElementById('secondChart'));
option_proporcao_sexos = {
  title: {
	text: 'Proporção de casos ao ano'
  },
  color: ["#B9D8C2", "#84C8C2", "#2066A9", "#19407F"],
  tooltip: {
	trigger: 'axis',
	axisPointer: {
	  type: 'shadow'
	}
  },
  legend: {
	data: ['Masculino', 'Feminino - Gestante', 'Feminino - Não Gestante'],
	bottom: '-7'
  },
  toolbox: {
	show: true,
	feature: {
		restore: {show: true, title: 'Restaurar'},
		saveAsImage: {show: true, title: 'Baixar'}
	}
  },
  grid: {
	left: '3%',
	right: '4%',
	bottom: '3%',
	containLabel: true
  },
  xAxis: {
	type: 'value',
    max: 100,
    axisLabel: {formatter: '{value}%'}
  },
  yAxis: {
	type: 'category',
	data: ['2007', '2008', '2009', '2010', '2011', '2012', '2013']
  },
  series: [
	{
	  name: 'Masculino',
	  type: 'bar',
	  stack: '1',
	  label: {
		show: true,
		position: 'insideRight'
	  },
	  data: [18, 18, 18, 29, 10, 18, 20]
	},
	{
	  name: 'Feminino - Gestante',
	  type: 'bar',
	  stack: '1',
	  label: {
		show: true,
		position: 'insideRight'
	  },
	  data: [10, 8, 12, 35, 30, 20, 10]
	},
	{
	  name: 'Feminino - Não Gestante',
	  type: 'bar',
	  stack: '1',
	  label: {
		show: true,
		position: 'insideRight'
	  },
	  data: [72, 74, 70, 36, 60, 62, 70]
	}
  ]
};

$.ajax({
	dataType: "json",
	url: "http://127.0.0.1:5000/sifilis/ano/sexo",
	success: function (data) {
	  var l_ano = [];
	  var l_data_f = [];
	  var l_data_m = [];
	  var l_data_g = [];

      for (var key_d in data['F']) {
        l_ano.push(key_d);
        l_data_f.push(data['F'][key_d]);
		l_data_m.push(data['M'][key_d]);
		l_data_g.push(data['G'][key_d]);
	  }
	  option_proporcao_sexos.yAxis.data = l_ano;
	  option_proporcao_sexos.series[0].data = l_data_m;
	  option_proporcao_sexos.series[1].data = l_data_g;
	  option_proporcao_sexos.series[2].data = l_data_f;
	  secondChart.setOption(option_proporcao_sexos);
	}
  });


// // Incidencias e Obitos
// var thrirdChart = echarts.init(document.getElementById('thrirdChart'));
// option_incidencia_obito = {
// 	title: {
// 		text: 'Incidência e óbito por ano'
// 	},
// 	color: ['#dca18e', '#8cc1b5', '#B9D8C2', '#84C8C2'],
// 	tooltip: {
// 		trigger: 'axis',
// 		axisPointer: {
// 			type: 'cross',
// 			label: {
// 				backgroundColor: '#6a7985'
// 			}
// 		}
// 	},
// 	legend: {
// 		data: ['Incidência', 'Óbito'],
// 		bottom: '-5'
// 	},
// 	toolbox: {
// 		show: true,
// 		feature: {
// 			restore: {show: true, title: 'Restaurar'},
// 			saveAsImage: {show: true, title: 'Baixar'}
// 		}
// 	},
// 	grid: {
// 		left: '3%',
// 		right: '4%',
// 		bottom: '3%',
// 		containLabel: true
// 	},
// 	xAxis: [
// 		{
// 			type: 'category',
// 			boundaryGap: false,
// 			data: ['2010', '2011', '2012', '2013', '2014', '2015', '2016']
// 		}
// 	],
// 	yAxis: [
// 		{
// 			type: 'value'
// 		}
// 	],
// 	series: [
// 		{
// 			name: 'Óbito',
// 			type: 'line',
// 			stack: '1',
// 			areaStyle: {},
// 			data: [120, 132, 101, 134, 90, 230, 210]
// 		},
// 		{
// 			name: 'Incidência',
// 			type: 'line',
// 			stack: '1',
// 			areaStyle: {},
// 			data: [220, 182, 191, 234, 290, 330, 310]
// 		}
// 	]
// };
// thrirdChart.setOption(option_incidencia_obito);

// // Média de proporção de obitos
// var fourthChart = echarts.init(document.getElementById('fourthChart'));
// option_proporcao_obito = {
//     title: {
//         text: 'Proporção de óbitos',
//         subtext: 'Entre os anos 2010 à 2016',
//         left: 'center'
// 	},
// 	color: ['#dca18e', '#b9d8c2'],
//     tooltip: {
//         trigger: 'item',
//         formatter: '{d}%'
// 	},
// 	legend: {
//         left: 'center',
//         bottom: 'bottom',
// 		data: ['Óbitos', 'Casos sem óbitos']
// 	},
//     series: [
//         {
//             name: 'Casos',
//             type: 'pie',
//             radius: '85%',
// 			center: ['50%', '53%'],
// 			labelLine: {
//                 show: false
//             },
//             data: [
//                 {value: 33, name: 'Óbitos', label: {show: false}},
//                 {value: 67, name: 'Casos sem óbitos', label: {show: false}}
//             ]
//         }
//     ]
// };
// fourthChart.setOption(option_proporcao_obito);


// Enables responsive charts
$(window).on('resize', function(){
  if(firstChart != null && firstChart != undefined){
	firstChart.resize();
  }
  if(secondChart != null && secondChart != undefined){
	secondChart.resize();
  }
  if(thrirdChart != null && thrirdChart != undefined){
	thrirdChart.resize();
  }
  if(fourthChart != null && fourthChart != undefined){
	fourthChart.resize();
  }
});
