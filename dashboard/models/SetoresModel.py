from dashboard import app, db

class Setores(db.Model):
    __tablename__ = 'setores'

    id = db.Column(db.Integer, primary_key=True)
    lat = db.Column(db.Float())
    lon = db.Column(db.Float())
    area = db.Column(db.Float())
    population = db.Column(db.Integer)
    CD_GEOCODI = db.Column(db.String(), unique=True)

    def __init__(self, lat, lon, CD_GEOCODI, area, population):
        self.lat = lat
        self.lon = lon
        self.CD_GEOCODI = CD_GEOCODI
        self.area = area
        self.population = population

    def get_lat_long(geocode):
        data = db.session.query(Setores).filter_by(CD_GEOCODI=geocode).first()
        return data

    def __repr__(self):
        return '<id {}>'.format(self.id)