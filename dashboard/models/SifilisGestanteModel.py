from dashboard import app, db
from flask import Flask, render_template, request, jsonify
from sqlalchemy import func
import sqlalchemy
from sqlalchemy.sql.expression import cast
from sqlalchemy import or_

class SifilisGestante(db.Model):
    __tablename__ = 'sifilis_gestante'

    id = db.Column(db.Integer, primary_key=True, default=lambda: uuid.uuid4().hex)
    id_sinan_syphilis_gestational = db.Column(db.String())
    id_sinan_syphilis = db.Column(db.String())
    id_syphilis_type = db.Column(db.String())
    id_dataset_sinan = db.Column(db.String())
    syphilis_diagnosis = db.Column(db.String())
    dt_notific = db.Column(db.String())
    nu_ano = db.Column(db.String())
    dt_diag = db.Column(db.String())
    nu_idade_n = db.Column(db.String())
    female = db.Column(db.String())
    cs_gestant = db.Column(db.String())
    cs_sexo = db.Column(db.String())
    classi_fin = db.Column(db.String())
    tpconfirma = db.Column(db.String())
    nu_notific = db.Column(db.String())
    id_municip = db.Column(db.String())
    id_unidade = db.Column(db.String())
    id_regiona = db.Column(db.String())
    sg_uf_not = db.Column(db.String())
    cs_raca = db.Column(db.String())
    cs_escol_n = db.Column(db.String())
    nu_lote_v = db.Column(db.String())
    nu_lote_h = db.Column(db.String())
    nduplic_n = db.Column(db.String())
    tp_not = db.Column(db.String())
    id_agravo = db.Column(db.String())
    dt_digita = db.Column(db.String())
    dt_transus = db.Column(db.String())
    dt_transdm = db.Column(db.String())
    dt_transsm = db.Column(db.String())
    dt_transrm = db.Column(db.String())
    dt_transrs = db.Column(db.String())
    dt_transse = db.Column(db.String())
    cs_flxret = db.Column(db.String())
    ident_micr = db.Column(db.String())
    migrado_w = db.Column(db.String())
    id_ocupa_n = db.Column(db.String())
    pre_ufrel = db.Column(db.String())
    pre_munire = db.Column(db.String())
    pre_unipre = db.Column(db.String())
    pre_sispre = db.Column(db.String())
    tpevidenci = db.Column(db.String())
    tpteste1 = db.Column(db.String())
    dstitulo1 = db.Column(db.String())
    dtteste1 = db.Column(db.String())
    tpesquema = db.Column(db.String())
    tratparc = db.Column(db.String())
    tpesqpar = db.Column(db.String())
    tpmotparc = db.Column(db.String())
    dsmotivo = db.Column(db.String())
    ds_obs = db.Column(db.String())
    sem_diag = db.Column(db.String())
    geo_sifil_id_municipio_6d = db.Column(db.String())
    geo_sifil_id_municipio_7d = db.Column(db.String())
    geo_sifil_nm_municipio = db.Column(db.String())
    geo_sifil_code_tract = db.Column(db.String())
    geo_sifil_zone = db.Column(db.String())
    geo_sifil_code_muni = db.Column(db.String())
    geo_sifil_name_muni = db.Column(db.String())
    geo_sifil_name_neighborhood = db.Column(db.String())
    geo_sifil_code_neighborhood = db.Column(db.String())
    geo_sifil_code_subdistrict = db.Column(db.String())
    geo_sifil_name_subdistrict = db.Column(db.String())
    geo_sifil_code_district = db.Column(db.String())
    geo_sifil_name_district = db.Column(db.String())
    geo_sifil_code_state = db.Column(db.String())                

    def __init__(self,
                 id_sinan_syphilis_gestational,
                 id_sinan_syphilis,
                 id_syphilis_type,
                 id_dataset_sinan,
                 syphilis_diagnosis,
                 dt_notific,
                 nu_ano,
                 dt_diag,
                 nu_idade_n,
                 female,
                 cs_gestant,
                 cs_sexo,
                 classi_fin,
                 tpconfirma,
                 nu_notific,
                 id_municip,
                 id_unidade,
                 id_regiona,
                 sg_uf_not,
                 cs_raca,
                 cs_escol_n,
                 nu_lote_v,
                 nu_lote_h,
                 nduplic_n,
                 tp_not,
                 id_agravo,
                 dt_digita,
                 dt_transus,
                 dt_transdm,
                 dt_transsm,
                 dt_transrm,
                 dt_transrs,
                 dt_transse,
                 cs_flxret,
                 ident_micr,
                 migrado_w,
                 id_ocupa_n,
                 pre_ufrel,
                 pre_munire,
                 pre_unipre,
                 pre_sispre,
                 tpevidenci,
                 tpteste1,
                 dstitulo1,
                 dtteste1,
                 tpesquema,
                 tratparc,
                 tpesqpar,
                 tpmotparc,
                 dsmotivo,
                 ds_obs,
                 sem_diag,
                 geo_sifil_id_municipio_6d,
                 geo_sifil_id_municipio_7d,
                 geo_sifil_nm_municipio,
                 geo_sifil_code_tract,
                 geo_sifil_zone,
                 geo_sifil_code_muni,
                 geo_sifil_name_muni,
                 geo_sifil_name_neighborhood,
                 geo_sifil_code_neighborhood,
                 geo_sifil_code_subdistrict,
                 geo_sifil_name_subdistrict,
                 geo_sifil_code_district,
                 geo_sifil_name_district,
                 geo_sifil_code_state):
        self.id_sinan_syphilis_gestational = id_sinan_syphilis_gestational
        self.id_sinan_syphilis             = id_sinan_syphilis            
        self.id_syphilis_type              = id_syphilis_type             
        self.id_dataset_sinan              = id_dataset_sinan             
        self.syphilis_diagnosis            = syphilis_diagnosis           
        self.dt_notific                    = dt_notific                   
        self.nu_ano                        = nu_ano                       
        self.dt_diag                       = dt_diag                      
        self.nu_idade_n                    = nu_idade_n                   
        self.female                        = female                       
        self.cs_gestant                    = cs_gestant                   
        self.cs_sexo                       = cs_sexo                      
        self.classi_fin                    = classi_fin                   
        self.tpconfirma                    = tpconfirma                   
        self.nu_notific                    = nu_notific                   
        self.id_municip                    = id_municip                   
        self.id_unidade                    = id_unidade                   
        self.id_regiona                    = id_regiona                   
        self.sg_uf_not                     = sg_uf_not                    
        self.cs_raca                       = cs_raca                      
        self.cs_escol_n                    = cs_escol_n                   
        self.nu_lote_v                     = nu_lote_v                    
        self.nu_lote_h                     = nu_lote_h                    
        self.nduplic_n                     = nduplic_n                    
        self.tp_not                        = tp_not                       
        self.id_agravo                     = id_agravo                    
        self.dt_digita                     = dt_digita                    
        self.dt_transus                    = dt_transus                   
        self.dt_transdm                    = dt_transdm                   
        self.dt_transsm                    = dt_transsm                   
        self.dt_transrm                    = dt_transrm                   
        self.dt_transrs                    = dt_transrs                   
        self.dt_transse                    = dt_transse                   
        self.cs_flxret                     = cs_flxret                    
        self.ident_micr                    = ident_micr                   
        self.migrado_w                     = migrado_w                    
        self.id_ocupa_n                    = id_ocupa_n                   
        self.pre_ufrel                     = pre_ufrel                    
        self.pre_munire                    = pre_munire                   
        self.pre_unipre                    = pre_unipre                   
        self.pre_sispre                    = pre_sispre                   
        self.tpevidenci                    = tpevidenci                   
        self.tpteste1                      = tpteste1                     
        self.dstitulo1                     = dstitulo1                    
        self.dtteste1                      = dtteste1                     
        self.tpesquema                     = tpesquema                    
        self.tratparc                      = tratparc                     
        self.tpesqpar                      = tpesqpar                     
        self.tpmotparc                     = tpmotparc                    
        self.dsmotivo                      = dsmotivo                     
        self.ds_obs                        = ds_obs                       
        self.sem_diag                      = sem_diag                     
        self.geo_sifil_id_municipio_6d     = geo_sifil_id_municipio_6d    
        self.geo_sifil_id_municipio_7d     = geo_sifil_id_municipio_7d    
        self.geo_sifil_nm_municipio        = geo_sifil_nm_municipio       
        self.geo_sifil_code_tract          = geo_sifil_code_tract         
        self.geo_sifil_zone                = geo_sifil_zone               
        self.geo_sifil_code_muni           = geo_sifil_code_muni          
        self.geo_sifil_name_muni           = geo_sifil_name_muni          
        self.geo_sifil_name_neighborhood   = geo_sifil_name_neighborhood  
        self.geo_sifil_code_neighborhood   = geo_sifil_code_neighborhood  
        self.geo_sifil_code_subdistrict    = geo_sifil_code_subdistrict   
        self.geo_sifil_name_subdistrict    = geo_sifil_name_subdistrict   
        self.geo_sifil_code_district       = geo_sifil_code_district      
        self.geo_sifil_name_district       = geo_sifil_name_district      
        self.geo_sifil_code_state          = geo_sifil_code_state         

    def get_total_por_subdistrito():

        data = db.session.query(SifilisGestante.geo_sifil_code_subdistrict, func.count(1).label('total_sifilis')).group_by(SifilisGestante.geo_sifil_code_subdistrict).all()
        return data

    def get_total_sifilis_ano():
        data = dict(db.session.query(func.substr(SifilisGestante.dt_notific,6,9).label('ano'), func.count(1).label('total_sifilis')).group_by('ano').all())
        return data

    def get_value_list(form, parameter):
        values = form.getlist(parameter)
        if len(values) == 0:
            values = '*'
        return values

    def get_filtered_points(form):

        races = form.getlist('raca-cor')
        if len(races) == 0:
            races = ['1', '2', '3', '4', '5', '9', 'NA']

        genders = form.getlist('sexo')
        if len(genders) == 0:
            genders = ['F', 'M', 'I']

        ages = [0, 0]
        ageCode = '0'
        if form.getlist('faixa-etaria')[0] == 'menor1mes':
            ages = form.getlist('idade-dias')
            ageCode = '2'
            if len(ages) == 0:
                ages = [0, 31]
            else:
                ages = ages[0].split(',')
        elif form.getlist('faixa-etaria')[0] == 'menor1ano':
            ages = form.getlist('idade-meses')
            ageCode = '3'
            if len(ages) == 0:
                ages = [0, 12]
            else:
                ages = ages[0].split(',')
        elif form.getlist('faixa-etaria')[0] == 'maior1ano':
            ages = form.getlist('idade-anos')
            ageCode = '4'
            if len(ages) == 0:
                ages = [0, 100]
            else:
                ages = ages[0].split(',')

        periodo = []
        periodo = form.getlist('periodo')
        if len(periodo) == 0:
            periodo.append(1900)
            periodo.append(2030)
        else:
            periodo = periodo[0].split(',')

        distrito =  SifilisGestante.get_value_list(form, "distrito")

        subdistrito = SifilisGestante.get_value_list(form, "subdistrito")

        escolaridade = SifilisGestante.get_value_list(form, "escolaridade")

        bairro = SifilisGestante.get_value_list(form, 'bairro')

        diagnostico = SifilisGestante.get_value_list(form, 'diagnostico')

        points = db.session.query(SifilisGestante.geo_sifil_code_tract, func.count(1).label('total_sifilis')).\
                            filter(or_(SifilisGestante.syphilis_diagnosis.in_(diagnostico), diagnostico == '*')).\
                            filter(SifilisGestante.cs_raca.in_(races)).\
                            filter(SifilisGestante.cs_sexo.in_(genders)).\
                            filter(func.substr(SifilisGestante.nu_idade_n,1,1) == ageCode, func.substr(SifilisGestante.nu_idade_n,2,3).cast(sqlalchemy.Integer).between(ages[0],ages[1])).\
                            filter(func.substr(SifilisGestante.dt_notific,6,9).cast(sqlalchemy.Integer).between(periodo[0],periodo[1])).\
                            filter(or_(SifilisGestante.geo_sifil_code_district.in_(distrito), distrito == '*')).\
                            filter(or_(SifilisGestante.geo_sifil_code_subdistrict.in_(subdistrito), subdistrito == '*')).\
                            filter(or_(SifilisGestante.geo_sifil_code_neighborhood.in_(bairro), bairro == '*')).\
                            group_by(SifilisGestante.geo_sifil_code_tract).all()
        return points

    def get_neighborhoods():
        return dict(db.session.query(SifilisGestante.geo_sifil_code_neighborhood, SifilisGestante.geo_sifil_name_neighborhood).distinct())

    def __repr__(self):
        return '<id {}>'.format(self.id)
