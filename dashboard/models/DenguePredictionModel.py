from dashboard import app, db
from flask import Flask, render_template, request, jsonify
from sqlalchemy import func
import sqlalchemy
from sqlalchemy.sql.expression import cast
from sqlalchemy import or_

class DenguePrediction(db.Model):
    __tablename__ = 'dengue_prediction'

    id = db.Column(db.Integer, primary_key=True, default=lambda: uuid.uuid4().hex)
    n = db.Column(db.String())
    code_neighborhood = db.Column(db.String())
    name_neighborhood = db.Column(db.String())
    ano = db.Column(db.String())
    mes = db.Column(db.String())
    dengue_diagnosis = db.Column(db.String())

    def __init__(self,
                 n,
                 code_neighborhood,
                 name_neighborhood,
                 ano,
                 mes,
                 dengue_diagnosis):
        self.n                    = n
        self.code_neighborhood    = code_neighborhood            
        self.name_neighborhood    = name_neighborhood             
        self.ano                  = ano             
        self.mes                  = mes           
        self.dengue_diagnosis     = dengue_diagnosis

    def get_points(year, month):
        points = db.session.query(DenguePrediction.name_neighborhood, func.max(DenguePrediction.dengue_diagnosis).label('predicao')).\
                            filter(DenguePrediction.ano == str(year), DenguePrediction.mes == str(month)).\
                            group_by(DenguePrediction.name_neighborhood).all()
        return points

    def get_max_value(year):
        value = db.session.query(func.max(DenguePrediction.dengue_diagnosis.cast(sqlalchemy.Float))).\
                           filter(DenguePrediction.dengue_diagnosis != 'dengue_diagnosis').\
                           filter(DenguePrediction.ano == str(year)).scalar()
        return value

    def __repr__(self):
        return '<id {}>'.format(self.id)
