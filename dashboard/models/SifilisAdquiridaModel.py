from dashboard import app, db
from flask import Flask, render_template, request, jsonify
import json

#from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func
import sqlalchemy
from sqlalchemy.sql.expression import cast
from collections import Counter
from sqlalchemy import or_

class SifilisAdquirida(db.Model):
    __tablename__ = 'sifilis_adquirida'

    id = db.Column(db.Integer, primary_key=True, default=lambda: uuid.uuid4().hex)
    id_sinan_syphilis_acquired = db.Column(db.String())
    id_sinan_syphilis = db.Column(db.String())
    id_dataset_sinan = db.Column(db.String())
    syphilis_diagnosis = db.Column(db.String())
    dt_notific = db.Column(db.String())
    nu_ano = db.Column(db.String())
    dt_diag = db.Column(db.String())
    nu_idade = db.Column(db.String())
    female = db.Column(db.String())
    cs_gestant = db.Column(db.String())
    id_syphilis_type = db.Column(db.String())
    dt_sin_pri = db.Column(db.String())
    cs_sexo = db.Column(db.String())
    classi_fin = db.Column(db.String())
    id_dg_not = db.Column(db.String())
    dt_obito = db.Column(db.String())
    dt_encerra = db.Column(db.String())
    nu_notific = db.Column(db.String())
    sem_not = db.Column(db.String())
    id_municip = db.Column(db.String())
    id_unidade = db.Column(db.String())
    id_regiona = db.Column(db.String())
    sg_uf_not = db.Column(db.String())
    sem_pri = db.Column(db.String())
    cs_raca = db.Column(db.String())
    cs_escolar = db.Column(db.String())
    cs_escol_n = db.Column(db.String())
    id_ev_not = db.Column(db.String())
    evolucao = db.Column(db.String())
    nu_lote = db.Column(db.String())
    nu_lote_v = db.Column(db.String())
    nu_lote_h = db.Column(db.String())
    in_vincula = db.Column(db.String())
    nduplic = db.Column(db.String())
    nduplic_n = db.Column(db.String())
    id_agravo = db.Column(db.String())
    in_confirm = db.Column(db.String())
    cs_suspeit = db.Column(db.String())
    in_aids = db.Column(db.String())
    tp_not = db.Column(db.String())
    dt_invest = db.Column(db.String())
    criterio = db.Column(db.String())
    tpautocto = db.Column(db.String())
    coufinf = db.Column(db.String())
    copaisinf = db.Column(db.String())
    comuninf = db.Column(db.String())
    codisinf = db.Column(db.String())
    co_bainfc = db.Column(db.String())
    nobaiinf = db.Column(db.String())
    doenca_tra = db.Column(db.String())
    dt_digita = db.Column(db.String())
    dt_transus = db.Column(db.String())
    dt_transdm = db.Column(db.String())
    dt_transsm = db.Column(db.String())
    dt_transrm = db.Column(db.String())
    dt_transrs = db.Column(db.String())
    dt_transse = db.Column(db.String())
    cs_flxret = db.Column(db.String())
    flxrecebi = db.Column(db.String())
    ident_micr = db.Column(db.String())
    migrado_w = db.Column(db.String())
    cs_mening = db.Column(db.String())
    id_ocupa_n = db.Column(db.String())
    co_usucad = db.Column(db.String())
    co_usualt = db.Column(db.String())
    geo_sifil_id_municipio_6d = db.Column(db.String())
    geo_sifil_id_municipio_7d = db.Column(db.String())
    geo_sifil_nm_municipio = db.Column(db.String())
    geo_sifil_code_tract = db.Column(db.String())
    geo_sifil_zone = db.Column(db.String())
    geo_sifil_code_muni = db.Column(db.String())
    geo_sifil_name_muni = db.Column(db.String())
    geo_sifil_name_neighborhood = db.Column(db.String())
    geo_sifil_code_neighborhood = db.Column(db.String())
    geo_sifil_code_subdistrict = db.Column(db.String())
    geo_sifil_name_subdistrict = db.Column(db.String())
    geo_sifil_code_district = db.Column(db.String())
    geo_sifil_name_district = db.Column(db.String())
    geo_sifil_code_state = db.Column(db.String())

    def __init__(self, id_sinan_syphilis_acquired,
                 id_sinan_syphilis,
                 id_dataset_sinan,
                 syphilis_diagnosis,
                 dt_notific,
                 nu_ano,
                 dt_diag,
                 nu_idade,
                 female,
                 cs_gestant,
                 id_syphilis_type,
                 dt_sin_pri,
                 cs_sexo,
                 classi_fin,
                 id_dg_not,
                 dt_obito,
                 dt_encerra,
                 nu_notific,
                 sem_not,
                 id_municip,
                 id_unidade,
                 id_regiona,
                 sg_uf_not,
                 sem_pri,
                 cs_raca,
                 cs_escolar,
                 cs_escol_n,
                 id_ev_not,
                 evolucao,
                 nu_lote,
                 nu_lote_v,
                 nu_lote_h,
                 in_vincula,
                 nduplic,
                 nduplic_n,
                 id_agravo,
                 in_confirm,
                 cs_suspeit,
                 in_aids,
                 tp_not,
                 dt_invest,
                 criterio,
                 tpautocto,
                 coufinf,
                 copaisinf,
                 comuninf,
                 codisinf,
                 co_bainfc,
                 nobaiinf,
                 doenca_tra,
                 dt_digita,
                 dt_transus,
                 dt_transdm,
                 dt_transsm,
                 dt_transrm,
                 dt_transrs,
                 dt_transse,
                 cs_flxret,
                 flxrecebi,
                 ident_micr,
                 migrado_w,
                 cs_mening,
                 id_ocupa_n,
                 co_usucad,
                 co_usualt,
                 geo_sifil_id_municipio_6d,
                 geo_sifil_id_municipio_7d,
                 geo_sifil_nm_municipio,
                 geo_sifil_code_tract,
                 geo_sifil_zone,
                 geo_sifil_code_muni,
                 geo_sifil_name_muni,
                 geo_sifil_name_neighborhood,
                 geo_sifil_code_neighborhood,
                 geo_sifil_code_subdistrict,
                 geo_sifil_name_subdistrict,
                 geo_sifil_code_district,
                 geo_sifil_name_district,
                 geo_sifil_code_state):
        self.id_sinan_syphilis_acquired = id_sinan_syphilis_acquired
        self.id_sinan_syphilis = id_sinan_syphilis
        self.id_dataset_sinan = id_dataset_sinan
        self.syphilis_diagnosis = syphilis_diagnosis
        self.dt_notific = dt_notific
        self.nu_ano = nu_ano
        self.dt_diag = dt_diag
        self.nu_idade = nu_idade
        self.female = female
        self.cs_gestant = cs_gestant
        self.id_syphilis_type = id_syphilis_type
        self.dt_sin_pri = dt_sin_pri
        self.cs_sexo = cs_sexo
        self.classi_fin = classi_fin
        self.id_dg_not = id_dg_not
        self.dt_obito = dt_obito
        self.dt_encerra = dt_encerra
        self.nu_notific = nu_notific
        self.sem_not = sem_not
        self.id_municip = id_municip
        self.id_unidade = id_unidade
        self.id_regiona = id_regiona
        self.sg_uf_not = sg_uf_not
        self.sem_pri = sem_pri
        self.cs_raca = cs_raca
        self.cs_escolar = cs_escolar
        self.cs_escol_n = cs_escol_n
        self.id_ev_not = id_ev_not
        self.evolucao = evolucao
        self.nu_lote = nu_lote
        self.nu_lote_v = nu_lote_v
        self.nu_lote_h = nu_lote_h
        self.in_vincula = in_vincula
        self.nduplic = nduplic
        self.nduplic_n = nduplic_n
        self.id_agravo = id_agravo
        self.in_confirm = in_confirm
        self.cs_suspeit = cs_suspeit
        self.in_aids = in_aids
        self.tp_not = tp_not
        self.dt_invest = dt_invest
        self.criterio = criterio
        self.tpautocto = tpautocto
        self.coufinf = coufinf
        self.copaisinf = copaisinf
        self.comuninf = comuninf
        self.codisinf = codisinf
        self.co_bainfc = co_bainfc
        self.nobaiinf = nobaiinf
        self.doenca_tra = doenca_tra
        self.dt_digita = dt_digita
        self.dt_transus = dt_transus
        self.dt_transdm = dt_transdm
        self.dt_transsm = dt_transsm
        self.dt_transrm = dt_transrm
        self.dt_transrs = dt_transrs
        self.dt_transse = dt_transse
        self.cs_flxret = cs_flxret
        self.flxrecebi = flxrecebi
        self.ident_micr = ident_micr
        self.migrado_w = migrado_w
        self.cs_mening = cs_mening
        self.id_ocupa_n = id_ocupa_n
        self.co_usucad = co_usucad
        self.co_usualt = co_usualt
        self.geo_sifil_id_municipio_6d = geo_sifil_id_municipio_6d
        self.geo_sifil_id_municipio_7d = geo_sifil_id_municipio_7d
        self.geo_sifil_nm_municipio = geo_sifil_nm_municipio
        self.geo_sifil_code_tract = geo_sifil_code_tract
        self.geo_sifil_zone = geo_sifil_zone
        self.geo_sifil_code_muni = geo_sifil_code_muni
        self.geo_sifil_name_muni = geo_sifil_name_muni
        self.geo_sifil_name_neighborhood = geo_sifil_name_neighborhood
        self.geo_sifil_code_neighborhood = geo_sifil_code_neighborhood
        self.geo_sifil_code_subdistrict = geo_sifil_code_subdistrict
        self.geo_sifil_name_subdistrict = geo_sifil_name_subdistrict
        self.geo_sifil_code_district = geo_sifil_code_district
        self.geo_sifil_name_district = geo_sifil_name_district
        self.geo_sifil_code_state = geo_sifil_code_state

    def get_value_list(form, parameter):
        values = form.getlist(parameter)
        if len(values) == 0:
            values = '*'
        return values

    def get_filtered_points(form):

        races = form.getlist('raca-cor')
        if len(races) == 0:
            races = ['1', '2', '3', '4', '5', '9', 'NA']

        genders = form.getlist('sexo')
        if len(genders) == 0:
            genders = ['F', 'M', 'I']

        ages = [0, 0]
        ageCode = '0'
        if form.getlist('faixa-etaria')[0] == 'menor1mes':
            ages = form.getlist('idade-dias')
            ageCode = '2'
            if len(ages) == 0:
                ages = [0, 31]
            else:
                ages = ages[0].split(',')
        elif form.getlist('faixa-etaria')[0] == 'menor1ano':
            ages = form.getlist('idade-meses')
            ageCode = '3'
            if len(ages) == 0:
                ages = [0, 12]
            else:
                ages = ages[0].split(',')
        elif form.getlist('faixa-etaria')[0] == 'maior1ano':
            ages = form.getlist('idade-anos')
            ageCode = '4'
            if len(ages) == 0:
                ages = [0, 100]
            else:
                ages = ages[0].split(',')

        periodo = []
        periodo = form.getlist('periodo')
        if len(periodo) == 0:
            periodo.append(1900)
            periodo.append(2030)
        else:
            periodo = periodo[0].split(',')

        distrito =  SifilisAdquirida.get_value_list(form, "distrito")

        subdistrito = SifilisAdquirida.get_value_list(form, "subdistrito")

        escolaridade = SifilisAdquirida.get_value_list(form, "escolaridade")

        bairro = SifilisAdquirida.get_value_list(form, 'bairro')

        diagnostico = SifilisAdquirida.get_value_list(form, 'diagnostico')

        points = db.session.query(SifilisAdquirida.geo_sifil_code_tract, func.count(1).label('total_sifilis')).\
                            filter(or_(SifilisAdquirida.syphilis_diagnosis.in_(diagnostico), diagnostico == '*')).\
                            filter(SifilisAdquirida.cs_raca.in_(races)).\
                            filter(SifilisAdquirida.cs_sexo.in_(genders)).\
                            filter(func.substr(SifilisAdquirida.nu_idade,1,1) == ageCode, func.substr(SifilisAdquirida.nu_idade,2,3).cast(sqlalchemy.Integer).between(ages[0],ages[1])).\
                            filter(func.substr(SifilisAdquirida.dt_notific,6,9).cast(sqlalchemy.Integer).between(periodo[0],periodo[1])).\
                            filter(or_(SifilisAdquirida.geo_sifil_code_district.in_(distrito), distrito == '*')).\
                            filter(or_(SifilisAdquirida.geo_sifil_code_subdistrict.in_(subdistrito), subdistrito == '*')).\
                            filter(or_(SifilisAdquirida.geo_sifil_code_neighborhood.in_(bairro), bairro == '*')).\
                            group_by(SifilisAdquirida.geo_sifil_code_tract).all()
        return points

    def get_total_sifilis_subdistrito():
        data = db.session.query(SifilisAdquirida.geo_sifil_code_tract, func.count(1).label('total_sifilis')).group_by(SifilisAdquirida.geo_sifil_code_tract).all()
        return data

    def get_total_sifilis_ano():
        data = dict(db.session.query(func.substr(SifilisAdquirida.dt_notific,6,9).label('ano'), func.count(1).label('total_sifilis')).group_by('ano').all())
        return data

    def get_proporcao_sifilis_ano_sexo():
        i = 0
        data_f = dict(db.session.query(func.substr(SifilisAdquirida.dt_notific,6,9).label('ano'), func.count(1).label('total_sifilis')).filter(SifilisAdquirida.cs_sexo == 'F', func.substr(SifilisAdquirida.dt_notific,6,9).cast(sqlalchemy.Integer) >= 2000).group_by('ano').all())
        data_m = dict(db.session.query(func.substr(SifilisAdquirida.dt_notific,6,9).label('ano'), func.count(1).label('total_sifilis')).filter(SifilisAdquirida.cs_sexo == 'M', func.substr(SifilisAdquirida.dt_notific,6,9).cast(sqlalchemy.Integer) >= 2000).group_by('ano').all())

        return data_f, data_m

    def data_init():
        import psycopg2
        conn = psycopg2.connect(database="dados_sus", user="user_sus", password="pass", host="db", port="5432")
        cur = conn.cursor()
        with open('sinan_sifilis_adquirida_ime.csv', 'r', encoding="latin_1")  as f:
            cur.copy_from(f, 'sifilis_adquirida', sep=',', columns=['id_sinan_syphilis_acquired','id_sinan_syphilis','id_dataset_sinan','syphilis_diagnosis','dt_notific','nu_ano','dt_diag','nu_idade','female','cs_gestant','id_syphilis_type','dt_sin_pri','cs_sexo','classi_fin','id_dg_not','dt_obito','dt_encerra','nu_notific','sem_not','id_municip','id_unidade','id_regiona','sg_uf_not','sem_pri','cs_raca','cs_escolar','cs_escol_n','id_ev_not','evolucao','nu_lote','nu_lote_v','nu_lote_h','in_vincula','nduplic','nduplic_n','id_agravo','in_confirm','cs_suspeit','in_aids','tp_not','dt_invest','criterio','tpautocto','coufinf','copaisinf','comuninf','codisinf','co_bainfc','nobaiinf','doenca_tra','dt_digita','dt_transus','dt_transdm','dt_transsm','dt_transrm','dt_transrs','dt_transse','cs_flxret','flxrecebi','ident_micr','migrado_w','cs_mening','id_ocupa_n','co_usucad','co_usualt','geo_sifil_id_municipio_6d','geo_sifil_id_municipio_7d','geo_sifil_nm_municipio','geo_sifil_code_tract','geo_sifil_zone','geo_sifil_code_muni','geo_sifil_name_muni','geo_sifil_name_neighborhood','geo_sifil_code_neighborhood','geo_sifil_code_subdistrict','geo_sifil_name_subdistrict','geo_sifil_code_district','geo_sifil_name_district','geo_sifil_code_state'])
            conn.commit()
            return '201'
        return '404'

    def get_neighborhoods():
        return dict(db.session.query(SifilisAdquirida.geo_sifil_code_neighborhood, SifilisAdquirida.geo_sifil_name_neighborhood).distinct())

    def __repr__(self):
        return '<id {}>'.format(self.id)
