from dashboard import app, db

class Bairros(db.Model):
    __tablename__ = 'bairros'

    id = db.Column(db.Integer, primary_key=True)
    lat = db.Column(db.Float())
    lon = db.Column(db.Float())
    NM_BAIRRO = db.Column(db.String(), unique=True)

    def __init__(self, lat, lon, NM_BAIRRO):
        self.lat = lat
        self.lon = lon
        self.NM_BAIRRO = NM_BAIRRO

    def get_lat_long(name):
        data = db.session.query(Bairros).filter_by(NM_BAIRRO=name).first()
        return data

    def __repr__(self):
        return '<id {}>'.format(self.id)