from dashboard import app, db
from flask import Flask, render_template, request, jsonify
import json

from sqlalchemy import func
import sqlalchemy
from sqlalchemy.sql.expression import cast
from collections import Counter
from sqlalchemy import or_

class Sim(db.Model):
    __tablename__ = 'sim'

    id = db.Column(db.Integer, primary_key=True, default=lambda: uuid.uuid4().hex)
    id_sim                      = db.Column(db.String())
    id_dataset_sim              = db.Column(db.String())
    ano                         = db.Column(db.String())
    mes                         = db.Column(db.String())
    horaobito                   = db.Column(db.String())
    fetal_death                 = db.Column(db.String())
    infant_death                = db.Column(db.String())
    v_natural                   = db.Column(db.String())
    codmunnatu                  = db.Column(db.String())
    idade                       = db.Column(db.String())
    sexo                        = db.Column(db.String())
    female                      = db.Column(db.String())
    racacor                     = db.Column(db.String())
    estciv                      = db.Column(db.String())
    ocup                        = db.Column(db.String())
    esc                         = db.Column(db.String())
    esc2010                     = db.Column(db.String())
    seriescfal                  = db.Column(db.String())
    escfalagr1                  = db.Column(db.String())
    escfalagr2                  = db.Column(db.String())
    cnes                        = db.Column(db.String())
    lococor                     = db.Column(db.String())
    codestocor                  = db.Column(db.String())
    codmunocor                  = db.Column(db.String())
    baiocor                     = db.Column(db.String())
    codbaiocor                  = db.Column(db.String())
    endocor                     = db.Column(db.String())
    codendocor                  = db.Column(db.String())
    codregocor                  = db.Column(db.String())
    numendocor                  = db.Column(db.String())
    complocor                   = db.Column(db.String())
    cepocor                     = db.Column(db.String())
    m_causamat                  = db.Column(db.String())
    m_tpmorteoco                = db.Column(db.String())
    m_obitograv                 = db.Column(db.String())
    m_obitopuerp                = db.Column(db.String())
    cid_causabas                = db.Column(db.String())
    capcid_causabas             = db.Column(db.String())
    gpcid_causabas              = db.Column(db.String())
    icd10                       = db.Column(db.String())
    linhaa                      = db.Column(db.String())
    linhab                      = db.Column(db.String())
    linhac                      = db.Column(db.String())
    linhad                      = db.Column(db.String())
    linhaii                     = db.Column(db.String())
    cid_causaori                = db.Column(db.String())
    capcid_causaori             = db.Column(db.String())
    gpcid_causaori              = db.Column(db.String())
    linhaa_o                    = db.Column(db.String())
    linhab_o                    = db.Column(db.String())
    linhac_o                    = db.Column(db.String())
    linhad_o                    = db.Column(db.String())
    linhaii_o                   = db.Column(db.String())
    cid_causabas_regra          = db.Column(db.String())
    assistmed                   = db.Column(db.String())
    diag_exame                  = db.Column(db.String())
    diag_cirurgia               = db.Column(db.String())
    diag_necropsia              = db.Column(db.String())
    f_idademae                  = db.Column(db.String())
    f_escmae                    = db.Column(db.String())
    f_escmae2010                = db.Column(db.String())
    f_seriescmae                = db.Column(db.String())
    f_escmaeagr1                = db.Column(db.String())
    f_escmaeagr2                = db.Column(db.String())
    f_ocupmae                   = db.Column(db.String())
    f_qtdfilvivo                = db.Column(db.String())
    f_qtdfilmort                = db.Column(db.String())
    f_gravidez                  = db.Column(db.String())
    f_gestacao                  = db.Column(db.String())
    f_partonormal               = db.Column(db.String())
    f_obitoparto                = db.Column(db.String())
    f_peso                      = db.Column(db.String())
    f_nsemanagest               = db.Column(db.String())
    ext_circobito               = db.Column(db.String())
    ext_acidtrab                = db.Column(db.String())
    ext_fonte                   = db.Column(db.String())
    invest_obit                 = db.Column(db.String())
    invest_dt                   = db.Column(db.String())
    invest_fonte                = db.Column(db.String())
    med_atestante               = db.Column(db.String())
    med_dtatestado              = db.Column(db.String())
    med_comunsvoim              = db.Column(db.String())
    acid_descr                  = db.Column(db.String())
    acid_end                    = db.Column(db.String())
    acid_codend                 = db.Column(db.String())
    acid_numend                 = db.Column(db.String())
    acid_complend               = db.Column(db.String())
    acid_cep                    = db.Column(db.String())
    cart_codest                 = db.Column(db.String())
    cart_codmun                 = db.Column(db.String())
    cart_cod                    = db.Column(db.String())
    cart_numreg                 = db.Column(db.String())
    cart_dtreg                  = db.Column(db.String())
    r_cid_causabas              = db.Column(db.String())
    r_cid_caub                  = db.Column(db.String())
    r_icd10                     = db.Column(db.String())
    r_icd10b                    = db.Column(db.String())
    r_status                    = db.Column(db.String())
    r_explic                    = db.Column(db.String())
    r_compara_cb                = db.Column(db.String())
    r_nonres                    = db.Column(db.String())
    r_nonresc                   = db.Column(db.String())
    r_dtressele                 = db.Column(db.String())
    adm_retroalim               = db.Column(db.String())
    adm_dtcadastro              = db.Column(db.String())
    adm_numerolote              = db.Column(db.String())
    geo_obit_id_municipio_6d    = db.Column(db.String())
    geo_obit_id_municipio_7d    = db.Column(db.String())
    geo_obit_nm_municipio       = db.Column(db.String())
    geo_obit_code_tract         = db.Column(db.String())
    geo_obit_zone               = db.Column(db.String())
    geo_obit_code_muni          = db.Column(db.String())
    geo_obit_name_muni          = db.Column(db.String())
    geo_obit_name_neighborhood  = db.Column(db.String())
    geo_obit_code_neighborhood  = db.Column(db.String())
    geo_obit_code_subdistrict   = db.Column(db.String())
    geo_obit_name_subdistrict   = db.Column(db.String())
    geo_obit_code_district      = db.Column(db.String())
    geo_obit_name_district      = db.Column(db.String())
    geo_obit_code_state         = db.Column(db.String())

    def __init__(self,id_sim,
                 id_dataset_sim,
                 ano,
                 mes,
                 horaobito,
                 fetal_death,
                 infant_death,
                 v_natural,
                 codmunnatu,
                 idade,
                 sexo,
                 female,
                 racacor,
                 estciv,
                 ocup,
                 esc,
                 esc2010,
                 seriescfal,
                 escfalagr1,
                 escfalagr2,
                 cnes,
                 lococor,
                 codestocor,
                 codmunocor,
                 baiocor,
                 codbaiocor,
                 endocor,
                 codendocor,
                 codregocor,
                 numendocor,
                 complocor,
                 cepocor,
                 m_causamat,
                 m_tpmorteoco,
                 m_obitograv,
                 m_obitopuerp,
                 cid_causabas,
                 capcid_causabas,
                 gpcid_causabas,
                 icd10,
                 linhaa,
                 linhab,
                 linhac,
                 linhad,
                 linhaii,
                 cid_causaori,
                 capcid_causaori,
                 gpcid_causaori,
                 linhaa_o,
                 linhab_o,
                 linhac_o,
                 linhad_o,
                 linhaii_o,
                 cid_causabas_regra,
                 assistmed,
                 diag_exame,
                 diag_cirurgia,
                 diag_necropsia,
                 f_idademae,
                 f_escmae,
                 f_escmae2010,
                 f_seriescmae,
                 f_escmaeagr1,
                 f_escmaeagr2,
                 f_ocupmae,
                 f_qtdfilvivo,
                 f_qtdfilmort,
                 f_gravidez,
                 f_gestacao,
                 f_partonormal,
                 f_obitoparto,
                 f_peso,
                 f_nsemanagest,
                 ext_circobito,
                 ext_acidtrab,
                 ext_fonte,
                 invest_obit,
                 invest_dt,
                 invest_fonte,
                 med_atestante,
                 med_dtatestado,
                 med_comunsvoim,
                 acid_descr,
                 acid_end,
                 acid_codend,
                 acid_numend,
                 acid_complend,
                 acid_cep,
                 cart_codest,
                 cart_codmun,
                 cart_cod,
                 cart_numreg,
                 cart_dtreg,
                 r_cid_causabas,
                 r_cid_caub,
                 r_icd10,
                 r_icd10b,
                 r_status,
                 r_explic,
                 r_compara_cb,
                 r_nonres,
                 r_nonresc,
                 r_dtressele,
                 adm_retroalim,
                 adm_dtcadastro,
                 adm_numerolote,
                 geo_obit_id_municipio_6d,
                 geo_obit_id_municipio_7d,
                 geo_obit_nm_municipio,
                 geo_obit_code_tract,
                 geo_obit_zone,
                 geo_obit_code_muni,
                 geo_obit_name_muni,
                 geo_obit_name_neighborhood,
                 geo_obit_code_neighborhood,
                 geo_obit_code_subdistrict,
                 geo_obit_name_subdistrict,
                 geo_obit_code_district,
                 geo_obit_name_district,
                 geo_obit_code_state):
        self.id_sim = id_sim                     
        self.id_dataset_sim = id_dataset_sim            
        self.ano = ano                       
        self.mes = mes                       
        self.horaobito = horaobito                 
        self.fetal_death = fetal_death               
        self.infant_death = infant_death              
        self.v_natural = v_natural                   
        self.codmunnatu = codmunnatu                
        self.idade = idade                     
        self.sexo = sexo                      
        self.female = female                    
        self.racacor = racacor                   
        self.estciv = estciv                    
        self.ocup = ocup                      
        self.esc = esc                       
        self.esc2010 = esc2010                   
        self.seriescfal = seriescfal                
        self.escfalagr1 = escfalagr1                
        self.escfalagr2 = escfalagr2                
        self.cnes = cnes                      
        self.lococor = lococor                   
        self.codestocor = codestocor                
        self.codmunocor = codmunocor                
        self.baiocor = baiocor                   
        self.codbaiocor = codbaiocor                
        self.endocor = endocor                   
        self.codendocor = codendocor                
        self.codregocor = codregocor                
        self.numendocor = numendocor                
        self.complocor = complocor                 
        self.cepocor = cepocor                   
        self.m_causamat = m_causamat                
        self.m_tpmorteoco = m_tpmorteoco              
        self.m_obitograv = m_obitograv               
        self.m_obitopuerp = m_obitopuerp              
        self.cid_causabas = cid_causabas              
        self.capcid_causabas = capcid_causabas           
        self.gpcid_causabas = gpcid_causabas            
        self.icd10 = icd10                     
        self.linhaa = linhaa                    
        self.linhab = linhab                    
        self.linhac = linhac                    
        self.linhad = linhad                    
        self.linhaii = linhaii                   
        self.cid_causaori = cid_causaori              
        self.capcid_causaori = capcid_causaori           
        self.gpcid_causaori = gpcid_causaori            
        self.linhaa_o = linhaa_o                  
        self.linhab_o = linhab_o                  
        self.linhac_o = linhac_o                  
        self.linhad_o = linhad_o                  
        self.linhaii_o = linhaii_o                 
        self.cid_causabas_regra = cid_causabas_regra        
        self.assistmed = assistmed                 
        self.diag_exame = diag_exame                
        self.diag_cirurgia = diag_cirurgia             
        self.diag_necropsia = diag_necropsia            
        self.f_idademae = f_idademae                
        self.f_escmae = f_escmae                  
        self.f_escmae2010 = f_escmae2010              
        self.f_seriescmae = f_seriescmae              
        self.f_escmaeagr1 = f_escmaeagr1              
        self.f_escmaeagr2 = f_escmaeagr2              
        self.f_ocupmae = f_ocupmae                 
        self.f_qtdfilvivo = f_qtdfilvivo              
        self.f_qtdfilmort = f_qtdfilmort              
        self.f_gravidez = f_gravidez                
        self.f_gestacao = f_gestacao                
        self.f_partonormal = f_partonormal             
        self.f_obitoparto = f_obitoparto              
        self.f_peso = f_peso                    
        self.f_nsemanagest = f_nsemanagest             
        self.ext_circobito = ext_circobito             
        self.ext_acidtrab = ext_acidtrab              
        self.ext_fonte = ext_fonte                 
        self.invest_obit = invest_obit               
        self.invest_dt = invest_dt                 
        self.invest_fonte = invest_fonte              
        self.med_atestante = med_atestante             
        self.med_dtatestado = med_dtatestado            
        self.med_comunsvoim = med_comunsvoim            
        self.acid_descr = acid_descr                
        self.acid_end = acid_end                  
        self.acid_codend = acid_codend               
        self.acid_numend = acid_numend               
        self.acid_complend = acid_complend             
        self.acid_cep = acid_cep                  
        self.cart_codest = cart_codest               
        self.cart_codmun = cart_codmun               
        self.cart_cod = cart_cod                  
        self.cart_numreg = cart_numreg               
        self.cart_dtreg = cart_dtreg                
        self.r_cid_causabas = r_cid_causabas            
        self.r_cid_caub = r_cid_caub                
        self.r_icd10 = r_icd10                   
        self.r_icd10b = r_icd10b                  
        self.r_status = r_status                  
        self.r_explic = r_explic                  
        self.r_compara_cb = r_compara_cb              
        self.r_nonres = r_nonres                  
        self.r_nonresc = r_nonresc                 
        self.r_dtressele = r_dtressele               
        self.adm_retroalim = adm_retroalim             
        self.adm_dtcadastro = adm_dtcadastro            
        self.adm_numerolote = adm_numerolote            
        self.geo_obit_id_municipio_6d = geo_obit_id_municipio_6d  
        self.geo_obit_id_municipio_7d = geo_obit_id_municipio_7d  
        self.geo_obit_nm_municipio = geo_obit_nm_municipio     
        self.geo_obit_code_tract = geo_obit_code_tract       
        self.geo_obit_zone = geo_obit_zone             
        self.geo_obit_code_muni = geo_obit_code_muni        
        self.geo_obit_name_muni = geo_obit_name_muni        
        self.geo_obit_name_neighborhood = geo_obit_name_neighborhood
        self.geo_obit_code_neighborhood = geo_obit_code_neighborhood
        self.geo_obit_code_subdistrict = geo_obit_code_subdistrict 
        self.geo_obit_name_subdistrict = geo_obit_name_subdistrict 
        self.geo_obit_code_district = geo_obit_code_district    
        self.geo_obit_name_district = geo_obit_name_district    
        self.geo_obit_code_state = geo_obit_code_state       

    def get_value_list(form, parameter):
        values = form.getlist(parameter)
        if len(values) == 0:
            if(parameter == 'causa'):
                values=['A50', 'A500', 'A501', 'A502', 'A504', 'A505', 'A506', 'A507', 'A509', 'A51', 'A510', 'A511', 'A512', 'A513', 'A514', 'A515', 'A519', 'A52', 'A520', 'A521', 'A522', 'A523', 'A527', 'A528', 'A529', 'A53', 'A530', 'A539', 'O981']
            else:
                values = '*'
        return values

    def get_filtered_points(form):

        races = form.getlist('raca-cor')
        if len(races) == 0:
            races = ['1', '2', '3', '4', '5', '9', 'NA', '']

        genders = form.getlist('sexo')
        if len(genders) == 0:
            genders = ['F', 'M', 'I']

        periodo = []
        periodo = form.getlist('periodo')
        if len(periodo) == 0:
            periodo.append(1900)
            periodo.append(2030)
        else:
            periodo = periodo[0].split(',')

        distrito =  Sim.get_value_list(form, "distrito")

        subdistrito = Sim.get_value_list(form, "subdistrito")

        escolaridade = Sim.get_value_list(form, "escolaridade")

        bairro = Sim.get_value_list(form, 'bairro')

        causa = Sim.get_value_list(form, 'causa')

        filtro_causa = []
        for c in causa:
            c_like = '%' + c + '%'
            filtro_causa.append(func.upper(Sim.cid_causabas).like(c_like))
            filtro_causa.append(func.upper(Sim.cid_causaori).like(c_like))
            filtro_causa.append(func.upper(Sim.linhaa).like(c_like))
            filtro_causa.append(func.upper(Sim.linhab).like(c_like))
            filtro_causa.append(func.upper(Sim.linhac).like(c_like))
            filtro_causa.append(func.upper(Sim.linhad).like(c_like))
            filtro_causa.append(func.upper(Sim.icd10).like(c_like))
            filtro_causa.append(func.upper(Sim.linhaii).like(c_like))
            filtro_causa.append(func.upper(Sim.linhaa_o).like(c_like))
            filtro_causa.append(func.upper(Sim.linhab_o).like(c_like))
            filtro_causa.append(func.upper(Sim.linhac_o).like(c_like))
            filtro_causa.append(func.upper(Sim.linhad_o).like(c_like))
            filtro_causa.append(func.upper(Sim.linhaii_o).like(c_like))
            filtro_causa.append(func.upper(Sim.icd10).like(c_like))
            filtro_causa.append(func.upper(Sim.capcid_causabas).like(c_like))
            filtro_causa.append(func.upper(Sim.capcid_causaori).like(c_like))
            filtro_causa.append(func.upper(Sim.gpcid_causabas).like(c_like))
            filtro_causa.append(func.upper(Sim.gpcid_causaori).like(c_like))
            filtro_causa.append(func.upper(Sim.r_cid_causabas).like(c_like))
            filtro_causa.append(func.upper(Sim.r_icd10).like(c_like))
            filtro_causa.append(func.upper(Sim.r_cid_caub).like(c_like))
            filtro_causa.append(func.upper(Sim.r_icd10b).like(c_like))

        points = db.session.query(Sim.geo_obit_code_tract, func.count(1).label('total_sifilis')).\
                            filter(Sim.racacor.in_(races)).\
                            filter(Sim.sexo.in_(genders)).\
                            filter(Sim.ano.cast(sqlalchemy.Integer).between(periodo[0],periodo[1])).\
                            filter(or_(Sim.geo_obit_code_district.in_(distrito), distrito == '*')).\
                            filter(or_(Sim.geo_obit_code_subdistrict.in_(subdistrito), subdistrito == '*')).\
                            filter(or_(Sim.geo_obit_code_neighborhood.in_(bairro), bairro == '*')).\
                            filter(or_(*filtro_causa)).\
                            group_by(Sim.geo_obit_code_tract).all()
        return points

    def get_neighborhoods():
        return dict(db.session.query(Sim.geo_obit_code_neighborhood, Sim.geo_obit_name_neighborhood).distinct())

    def __repr__(self):
        return '<id {}>'.format(self.id)
