from dashboard import app, db
from flask import Flask, render_template, request, jsonify
from sqlalchemy import func
import sqlalchemy
from sqlalchemy.sql.expression import cast
from sqlalchemy import or_

class SifilisCongenita(db.Model):
    __tablename__ = 'sifilis_congenita'

    id = db.Column(db.Integer, primary_key=True, default=lambda: uuid.uuid4().hex)
    id_sinan_syphilis_congenital = db.Column(db.String())
    id_sinan_syphilis = db.Column(db.String())
    id_dataset_sinan = db.Column(db.String())
    syphilis_diagnosis = db.Column(db.String())
    dt_notific = db.Column(db.String())
    nu_ano = db.Column(db.String())
    dt_diag = db.Column(db.String())
    nu_idade = db.Column(db.String())
    female = db.Column(db.String())
    cs_gestant = db.Column(db.String())
    id_syphilis_type = db.Column(db.String())
    dt_sin_pri = db.Column(db.String())
    ant_dt_nas = db.Column(db.String())
    ant_idade = db.Column(db.String())
    cs_sexo = db.Column(db.String())
    evo_diag_f = db.Column(db.String())
    evo_diag_n = db.Column(db.String())
    id_dg_not = db.Column(db.String())
    dt_obito = db.Column(db.String())
    dt_encerra = db.Column(db.String())
    nu_notific = db.Column(db.String())
    sem_not = db.Column(db.String())
    id_municip = db.Column(db.String())
    id_unidade = db.Column(db.String())
    id_regiona = db.Column(db.String())
    sg_uf_not = db.Column(db.String())
    sem_pri = db.Column(db.String())
    cs_raca = db.Column(db.String())
    cs_escolar = db.Column(db.String())
    cs_escol_n = db.Column(db.String())
    id_ev_not = db.Column(db.String())
    evolucao = db.Column(db.String())
    nu_lote = db.Column(db.String())
    nu_lote_v = db.Column(db.String())
    nu_lote_h = db.Column(db.String())
    in_vincula = db.Column(db.String())
    nduplic = db.Column(db.String())
    nduplic_n = db.Column(db.String())
    dt_invest = db.Column(db.String())
    id_ocupaca = db.Column(db.String())
    escol_mae = db.Column(db.String())
    ant_gestac = db.Column(db.String())
    ant_partos = db.Column(db.String())
    ant_aborto = db.Column(db.String())
    ant_natimo = db.Column(db.String())
    ant_pre_na = db.Column(db.String())
    ant_porque = db.Column(db.String())
    ant_local = db.Column(db.String())
    ant_consul = db.Column(db.String())
    ant_semana = db.Column(db.String())
    ant_meses = db.Column(db.String())
    ant_sifili = db.Column(db.String())
    ant_forma = db.Column(db.String())
    ant_parcei = db.Column(db.String())
    ant_tratad = db.Column(db.String())
    ant_eviden = db.Column(db.String())
    lab_vdrl1 = db.Column(db.String())
    lab_titulo = db.Column(db.String())
    lab_dt1 = db.Column(db.String())
    lab_vdrl2 = db.Column(db.String())
    lab_titu_1 = db.Column(db.String())
    lab_dt2 = db.Column(db.String())
    lab_parto = db.Column(db.String())
    lab_titu_2 = db.Column(db.String())
    lab_dt3 = db.Column(db.String())
    lab_conf = db.Column(db.String())
    lab_diag = db.Column(db.String())
    tra_esquem = db.Column(db.String())
    tra_dt = db.Column(db.String())
    ant_uf_cri = db.Column(db.String())
    ant_muni_c = db.Column(db.String())
    ant_local_ = db.Column(db.String())
    ant_sema_1 = db.Column(db.String())
    ant_meses_ = db.Column(db.String())
    ant_peso_c = db.Column(db.String())
    ant_abor_1 = db.Column(db.String())
    ant_nati_1 = db.Column(db.String())
    ant_obito_ = db.Column(db.String())
    ant_dt_obi = db.Column(db.String())
    ant_exphiv = db.Column(db.String())
    cli_assint = db.Column(db.String())
    cli_icteri = db.Column(db.String())
    cli_rinite = db.Column(db.String())
    cli_anemia = db.Column(db.String())
    cli_hepato = db.Column(db.String())
    cli_esplen = db.Column(db.String())
    cli_lesoes = db.Column(db.String())
    cli_osteo = db.Column(db.String())
    cli_pseudo = db.Column(db.String())
    cli_outro = db.Column(db.String())
    cli_desc_o = db.Column(db.String())
    labc_umbil = db.Column(db.String())
    labc_titul = db.Column(db.String())
    labc_igm = db.Column(db.String())
    labc_sangu = db.Column(db.String())
    labc_tit_1 = db.Column(db.String())
    labc_dt_1 = db.Column(db.String())
    labc_igg = db.Column(db.String())
    labc_liquo = db.Column(db.String())
    labc_tit_2 = db.Column(db.String())
    labc_dt_2 = db.Column(db.String())
    labc_tpha = db.Column(db.String())
    labc_evide = db.Column(db.String())
    labc_liq_1 = db.Column(db.String())
    labc_outro = db.Column(db.String())
    tra_diag_t = db.Column(db.String())
    tra_esqu_1 = db.Column(db.String())
    tra_dt_tra = db.Column(db.String())
    evo_diagno = db.Column(db.String())
    labc_dt_3 = db.Column(db.String())
    labc_hiv = db.Column(db.String())
    in_aids = db.Column(db.String())
    tp_not = db.Column(db.String())
    id_agravo = db.Column(db.String())
    dt_digita = db.Column(db.String())
    dt_transus = db.Column(db.String())
    dt_transdm = db.Column(db.String())
    dt_transsm = db.Column(db.String())
    dt_transrm = db.Column(db.String())
    dt_transrs = db.Column(db.String())
    dt_transse = db.Column(db.String())
    cs_flxret = db.Column(db.String())
    ident_micr = db.Column(db.String())
    migrado_w = db.Column(db.String())
    ant_raca = db.Column(db.String())
    id_ocupa_n = db.Column(db.String())
    escolmae = db.Column(db.String())
    uf_pre_nat = db.Column(db.String())
    mun_pre_na = db.Column(db.String())
    uni_pre_na = db.Column(db.String())
    antsifil_n = db.Column(db.String())
    labc_dt = db.Column(db.String())
    hepato = db.Column(db.String())
    lesoes = db.Column(db.String())
    sin_outr_e = db.Column(db.String())
    ds_esquema = db.Column(db.String())
    ds_obs = db.Column(db.String())
    sem_diag = db.Column(db.String())
    geo_sifil_id_municipio_6d = db.Column(db.String())
    geo_sifil_id_municipio_7d = db.Column(db.String())
    geo_sifil_nm_municipio = db.Column(db.String())
    geo_sifil_code_tract = db.Column(db.String())
    geo_sifil_zone = db.Column(db.String())
    geo_sifil_code_muni = db.Column(db.String())
    geo_sifil_name_muni = db.Column(db.String())
    geo_sifil_name_neighborhood = db.Column(db.String())
    geo_sifil_code_neighborhood = db.Column(db.String())
    geo_sifil_code_subdistrict = db.Column(db.String())
    geo_sifil_name_subdistrict = db.Column(db.String())
    geo_sifil_code_district = db.Column(db.String())
    geo_sifil_name_district = db.Column(db.String())
    geo_sifil_code_state = db.Column(db.String())                  

    def __init__(self,
                 id_sinan_syphilis_congenital,
                 id_sinan_syphilis,
                 id_dataset_sinan,
                 syphilis_diagnosis,
                 dt_notific,
                 nu_ano,
                 dt_diag,
                 nu_idade,
                 female,
                 cs_gestant,
                 id_syphilis_type,
                 dt_sin_pri,
                 ant_dt_nas,
                 ant_idade,
                 cs_sexo,
                 evo_diag_f,
                 evo_diag_n,
                 id_dg_not,
                 dt_obito,
                 dt_encerra,
                 nu_notific,
                 sem_not,
                 id_municip,
                 id_unidade,
                 id_regiona,
                 sg_uf_not,
                 sem_pri,
                 cs_raca,
                 cs_escolar,
                 cs_escol_n,
                 id_ev_not,
                 evolucao,
                 nu_lote,
                 nu_lote_v,
                 nu_lote_h,
                 in_vincula,
                 nduplic,
                 nduplic_n,
                 dt_invest,
                 id_ocupaca,
                 escol_mae,
                 ant_gestac,
                 ant_partos,
                 ant_aborto,
                 ant_natimo,
                 ant_pre_na,
                 ant_porque,
                 ant_local,
                 ant_consul,
                 ant_semana,
                 ant_meses,
                 ant_sifili,
                 ant_forma,
                 ant_parcei,
                 ant_tratad,
                 ant_eviden,
                 lab_vdrl1,
                 lab_titulo,
                 lab_dt1,
                 lab_vdrl2,
                 lab_titu_1,
                 lab_dt2,
                 lab_parto,
                 lab_titu_2,
                 lab_dt3,
                 lab_conf,
                 lab_diag,
                 tra_esquem,
                 tra_dt,
                 ant_uf_cri,
                 ant_muni_c,
                 ant_local_,
                 ant_sema_1,
                 ant_meses_,
                 ant_peso_c,
                 ant_abor_1,
                 ant_nati_1,
                 ant_obito_,
                 ant_dt_obi,
                 ant_exphiv,
                 cli_assint,
                 cli_icteri,
                 cli_rinite,
                 cli_anemia,
                 cli_hepato,
                 cli_esplen,
                 cli_lesoes,
                 cli_osteo,
                 cli_pseudo,
                 cli_outro,
                 cli_desc_o,
                 labc_umbil,
                 labc_titul,
                 labc_igm,
                 labc_sangu,
                 labc_tit_1,
                 labc_dt_1,
                 labc_igg,
                 labc_liquo,
                 labc_tit_2,
                 labc_dt_2,
                 labc_tpha,
                 labc_evide,
                 labc_liq_1,
                 labc_outro,
                 tra_diag_t,
                 tra_esqu_1,
                 tra_dt_tra,
                 evo_diagno,
                 labc_dt_3,
                 labc_hiv,
                 in_aids,
                 tp_not,
                 id_agravo,
                 dt_digita,
                 dt_transus,
                 dt_transdm,
                 dt_transsm,
                 dt_transrm,
                 dt_transrs,
                 dt_transse,
                 cs_flxret,
                 ident_micr,
                 migrado_w,
                 ant_raca,
                 id_ocupa_n,
                 escolmae,
                 uf_pre_nat,
                 mun_pre_na,
                 uni_pre_na,
                 antsifil_n,
                 labc_dt,
                 hepato,
                 lesoes,
                 sin_outr_e,
                 ds_esquema,
                 ds_obs,
                 sem_diag,
                 geo_sifil_id_municipio_6d,
                 geo_sifil_id_municipio_7d,
                 geo_sifil_nm_municipio,
                 geo_sifil_code_tract,
                 geo_sifil_zone,
                 geo_sifil_code_muni,
                 geo_sifil_name_muni,
                 geo_sifil_name_neighborhood,
                 geo_sifil_code_neighborhood,
                 geo_sifil_code_subdistrict,
                 geo_sifil_name_subdistrict,
                 geo_sifil_code_district,
                 geo_sifil_name_district,
                 geo_sifil_code_state):
        self.id_sinan_syphilis_congenital = id_sinan_syphilis_congenital
        self.id_sinan_syphilis = id_sinan_syphilis           
        self.id_dataset_sinan = id_dataset_sinan            
        self.syphilis_diagnosis = syphilis_diagnosis          
        self.dt_notific = dt_notific                  
        self.nu_ano = nu_ano                      
        self.dt_diag = dt_diag                     
        self.nu_idade = nu_idade                    
        self.female = female                      
        self.cs_gestant = cs_gestant                  
        self.id_syphilis_type = id_syphilis_type            
        self.dt_sin_pri = dt_sin_pri                  
        self.ant_dt_nas = ant_dt_nas                  
        self.ant_idade = ant_idade                   
        self.cs_sexo = cs_sexo                     
        self.evo_diag_f = evo_diag_f                  
        self.evo_diag_n = evo_diag_n                  
        self.id_dg_not = id_dg_not                   
        self.dt_obito = dt_obito                    
        self.dt_encerra = dt_encerra                  
        self.nu_notific = nu_notific                  
        self.sem_not = sem_not                     
        self.id_municip = id_municip                  
        self.id_unidade = id_unidade                  
        self.id_regiona = id_regiona                  
        self.sg_uf_not = sg_uf_not                   
        self.sem_pri = sem_pri                     
        self.cs_raca = cs_raca                     
        self.cs_escolar = cs_escolar                  
        self.cs_escol_n = cs_escol_n                  
        self.id_ev_not = id_ev_not                   
        self.evolucao = evolucao                    
        self.nu_lote = nu_lote                     
        self.nu_lote_v = nu_lote_v                   
        self.nu_lote_h = nu_lote_h                   
        self.in_vincula = in_vincula                  
        self.nduplic = nduplic                     
        self.nduplic_n = nduplic_n                   
        self.dt_invest = dt_invest                   
        self.id_ocupaca = id_ocupaca                  
        self.escol_mae = escol_mae                   
        self.ant_gestac = ant_gestac                  
        self.ant_partos = ant_partos                  
        self.ant_aborto = ant_aborto                  
        self.ant_natimo = ant_natimo                  
        self.ant_pre_na = ant_pre_na                  
        self.ant_porque = ant_porque                  
        self.ant_local = ant_local                   
        self.ant_consul = ant_consul                  
        self.ant_semana = ant_semana                  
        self.ant_meses = ant_meses                   
        self.ant_sifili = ant_sifili                  
        self.ant_forma = ant_forma                   
        self.ant_parcei = ant_parcei                  
        self.ant_tratad = ant_tratad                  
        self.ant_eviden = ant_eviden                  
        self.lab_vdrl1 = lab_vdrl1                   
        self.lab_titulo = lab_titulo                  
        self.lab_dt1 = lab_dt1                     
        self.lab_vdrl2 = lab_vdrl2                   
        self.lab_titu_1 = lab_titu_1                  
        self.lab_dt2 = lab_dt2                     
        self.lab_parto = lab_parto                   
        self.lab_titu_2 = lab_titu_2                  
        self.lab_dt3 = lab_dt3                     
        self.lab_conf = lab_conf                    
        self.lab_diag = lab_diag                    
        self.tra_esquem = tra_esquem                  
        self.tra_dt = tra_dt                      
        self.ant_uf_cri = ant_uf_cri                  
        self.ant_muni_c = ant_muni_c                  
        self.ant_local_ = ant_local_                  
        self.ant_sema_1 = ant_sema_1                  
        self.ant_meses_ = ant_meses_                  
        self.ant_peso_c = ant_peso_c                  
        self.ant_abor_1 = ant_abor_1                  
        self.ant_nati_1 = ant_nati_1                  
        self.ant_obito_ = ant_obito_                  
        self.ant_dt_obi = ant_dt_obi                  
        self.ant_exphiv = ant_exphiv                  
        self.cli_assint = cli_assint                  
        self.cli_icteri = cli_icteri                  
        self.cli_rinite = cli_rinite                  
        self.cli_anemia = cli_anemia                  
        self.cli_hepato = cli_hepato                  
        self.cli_esplen = cli_esplen                  
        self.cli_lesoes = cli_lesoes                  
        self.cli_osteo = cli_osteo                   
        self.cli_pseudo = cli_pseudo                  
        self.cli_outro = cli_outro                   
        self.cli_desc_o = cli_desc_o                  
        self.labc_umbil = labc_umbil                  
        self.labc_titul = labc_titul                  
        self.labc_igm = labc_igm                    
        self.labc_sangu = labc_sangu                  
        self.labc_tit_1 = labc_tit_1                  
        self.labc_dt_1 = labc_dt_1                   
        self.labc_igg = labc_igg                    
        self.labc_liquo = labc_liquo                  
        self.labc_tit_2 = labc_tit_2                  
        self.labc_dt_2 = labc_dt_2                   
        self.labc_tpha = labc_tpha                   
        self.labc_evide = labc_evide                  
        self.labc_liq_1 = labc_liq_1                  
        self.labc_outro = labc_outro                  
        self.tra_diag_t = tra_diag_t                  
        self.tra_esqu_1 = tra_esqu_1                  
        self.tra_dt_tra = tra_dt_tra                  
        self.evo_diagno = evo_diagno                  
        self.labc_dt_3 = labc_dt_3                   
        self.labc_hiv = labc_hiv                    
        self.in_aids = in_aids                     
        self.tp_not = tp_not                      
        self.id_agravo = id_agravo                   
        self.dt_digita = dt_digita                   
        self.dt_transus = dt_transus                  
        self.dt_transdm = dt_transdm                  
        self.dt_transsm = dt_transsm                  
        self.dt_transrm = dt_transrm                  
        self.dt_transrs = dt_transrs                  
        self.dt_transse = dt_transse                  
        self.cs_flxret = cs_flxret                   
        self.ident_micr = ident_micr                  
        self.migrado_w = migrado_w                   
        self.ant_raca = ant_raca                    
        self.id_ocupa_n = id_ocupa_n                  
        self.escolmae = escolmae                    
        self.uf_pre_nat = uf_pre_nat                  
        self.mun_pre_na = mun_pre_na                  
        self.uni_pre_na = uni_pre_na                  
        self.antsifil_n = antsifil_n                  
        self.labc_dt = labc_dt                     
        self.hepato = hepato                      
        self.lesoes = lesoes                      
        self.sin_outr_e = sin_outr_e                  
        self.ds_esquema = ds_esquema                  
        self.ds_obs = ds_obs                      
        self.sem_diag = sem_diag                    
        self.geo_sifil_id_municipio_6d = geo_sifil_id_municipio_6d   
        self.geo_sifil_id_municipio_7d = geo_sifil_id_municipio_7d   
        self.geo_sifil_nm_municipio = geo_sifil_nm_municipio      
        self.geo_sifil_code_tract = geo_sifil_code_tract        
        self.geo_sifil_zone = geo_sifil_zone              
        self.geo_sifil_code_muni = geo_sifil_code_muni         
        self.geo_sifil_name_muni = geo_sifil_name_muni         
        self.geo_sifil_name_neighborhood = geo_sifil_name_neighborhood 
        self.geo_sifil_code_neighborhood = geo_sifil_code_neighborhood 
        self.geo_sifil_code_subdistrict = geo_sifil_code_subdistrict  
        self.geo_sifil_name_subdistrict = geo_sifil_name_subdistrict  
        self.geo_sifil_code_district = geo_sifil_code_district     
        self.geo_sifil_name_district = geo_sifil_name_district     
        self.geo_sifil_code_state = geo_sifil_code_state                

    def get_total_por_subdistrito():

        data = db.session.query(SifilisCongenita.geo_sifil_code_subdistrict, func.count(1).label('total_sifilis')).group_by(SifilisCongenita.geo_sifil_code_subdistrict).all()
        return data



    def get_total_sifilis_ano():
        data = dict(db.session.query(func.substr(SifilisCongenita.dt_notific,6,9).label('ano'), func.count(1).label('total_sifilis')).group_by('ano').all())
        return data

    def get_proporcao_sifilis_ano_sexo():
        i = 0
        data_f = dict(db.session.query(func.substr(SifilisCongenita.dt_notific,6,9).label('ano'), func.count(1).label('total_sifilis')).filter(SifilisCongenita.cs_sexo == 'F', func.substr(SifilisCongenita.dt_notific,6,9).cast(sqlalchemy.Integer) >= 2000).group_by('ano').all())
        data_m = dict(db.session.query(func.substr(SifilisCongenita.dt_notific,6,9).label('ano'), func.count(1).label('total_sifilis')).filter(SifilisCongenita.cs_sexo == 'M', func.substr(SifilisCongenita.dt_notific,6,9).cast(sqlalchemy.Integer) >= 2000).group_by('ano').all())

        return data_f, data_m

    def get_value_list(form, parameter):
        values = form.getlist(parameter)
        if len(values) == 0:
            values = '*'
        return values

    def get_filtered_points(form):

        races = form.getlist('raca-cor')
        if len(races) == 0:
            races = ['1', '2', '3', '4', '5', '9', 'NA']

        genders = form.getlist('sexo')
        if len(genders) == 0:
            genders = ['F', 'M', 'I']

        ages = [0, 0]
        ageCode = '0'
        if form.getlist('faixa-etaria')[0] == 'menor1mes':
            ages = form.getlist('idade-dias')
            ageCode = '2'
            if len(ages) == 0:
                ages = [0, 31]
            else:
                ages = ages[0].split(',')
        elif form.getlist('faixa-etaria')[0] == 'menor1ano':
            ages = form.getlist('idade-meses')
            ageCode = '3'
            if len(ages) == 0:
                ages = [0, 12]
            else:
                ages = ages[0].split(',')
        elif form.getlist('faixa-etaria')[0] == 'maior1ano':
            ages = form.getlist('idade-anos')
            ageCode = '4'
            if len(ages) == 0:
                ages = [0, 100]
            else:
                ages = ages[0].split(',')

        periodo = []
        periodo = form.getlist('periodo')
        if len(periodo) == 0:
            periodo.append(1900)
            periodo.append(2030)
        else:
            periodo = periodo[0].split(',')

        distrito =  SifilisCongenita.get_value_list(form, "distrito")

        subdistrito = SifilisCongenita.get_value_list(form, "subdistrito")

        escolaridade = SifilisCongenita.get_value_list(form, "escolaridade")

        bairro = SifilisCongenita.get_value_list(form, 'bairro')

        diagnostico = SifilisCongenita.get_value_list(form, 'diagnostico')

        points = db.session.query(SifilisCongenita.geo_sifil_code_tract, func.count(1).label('total_sifilis')).\
                            filter(or_(SifilisCongenita.syphilis_diagnosis.in_(diagnostico), diagnostico == '*')).\
                            filter(SifilisCongenita.cs_raca.in_(races)).\
                            filter(SifilisCongenita.cs_sexo.in_(genders)).\
                            filter(func.substr(SifilisCongenita.nu_idade,1,1) == ageCode, func.substr(SifilisCongenita.nu_idade,2,3).cast(sqlalchemy.Integer).between(ages[0],ages[1])).\
                            filter(func.substr(SifilisCongenita.dt_notific,6,9).cast(sqlalchemy.Integer).between(periodo[0],periodo[1])).\
                            filter(or_(SifilisCongenita.geo_sifil_code_district.in_(distrito), distrito == '*')).\
                            filter(or_(SifilisCongenita.geo_sifil_code_subdistrict.in_(subdistrito), subdistrito == '*')).\
                            filter(or_(SifilisCongenita.geo_sifil_code_neighborhood.in_(bairro), bairro == '*')).\
                            group_by(SifilisCongenita.geo_sifil_code_tract).all()
        return points

    def get_neighborhoods():
        return dict(db.session.query(SifilisCongenita.geo_sifil_code_neighborhood, SifilisCongenita.geo_sifil_name_neighborhood).distinct())

    def __repr__(self):
        return '<id {}>'.format(self.id)
