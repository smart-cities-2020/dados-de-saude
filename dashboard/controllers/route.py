from flask import Flask, render_template, request, jsonify
from dashboard import app, db
from dashboard import APP_ROOT
from dashboard.models.SifilisAdquiridaModel import SifilisAdquirida
from dashboard.models.SetoresModel import Setores
from dashboard.models.BairrosModel import Bairros
from dashboard.models.SifilisCongenitaModel import SifilisCongenita
from dashboard.models.SifilisGestanteModel import SifilisGestante
from dashboard.models.SimModel import Sim
from dashboard.models.DenguePredictionModel import DenguePrediction
from flask_cors import CORS, cross_origin
from collections import Counter

import requests
import json
import os


HEATMAP_TYPE = "number_cases"
DENSITY = 5265 # median density


def get_regions():
    result = dict()

    url = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios/3304557/distritos"
    data = json.loads(requests.get(url).text)
    result['districts'] = data

    url = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios/3304557/subdistritos"
    data = json.loads(requests.get(url).text)
    result['subdistricts'] = data

    # Provavelmente em breve a rota para 'Bairros' será liberada (https://servicodados.ibge.gov.br/api/docs/localidades?versao=1#api-releases)
    # url = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios/3304557/bairros"
    # data = json.loads(requests.get(url).text)
    # result['neighborhoods'] = data

    result['neighborhoods'] = get_neighborhoods()

    return result

@app.route('/')
@cross_origin()
def root():
    return render_template('index.html', points=[], region=get_regions(), heatmap_type=HEATMAP_TYPE)

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

def getBairros():
    result = dict()
    bairros = db.session.query(Bairros).all()
    for bairro in bairros:
        result[bairro.NM_BAIRRO] = [bairro.lat, bairro.lon]
    return result

def getMonth(x):
    month = 1
    if x >= 4 and x <= 8:
        month = 2
    elif x >= 13 and x <= 16:
        month = 3
    elif x >= 21 and x <= 24:
        month = 4
    elif x >= 29 and x <= 33:
        month = 5
    elif x >= 37 and x <= 41:
        month = 6
    elif x >= 46 and x <= 49:
        month = 7
    elif x >= 54 and x <= 58:
        month = 8
    elif x >= 62 and x <= 66:
        month = 9
    elif x >= 71 and x <= 74:
        month = 10
    elif x >= 79 and x <= 83:
        month = 11
    elif x >= 87 and x <= 91:
        month = 12

    return month

def getYearAndMonth(value):
    year_month = value.split('.')
    if len(year_month) > 1:
        year, month = value.split('.')[0], value.split('.')[1]
    else:
        year = value.split('.')[0]
        return year, 1

    year, month = int(year), int(month[0:2])

    if month >= 96:
        year = year + 1
        month = 1
    else:
        year = int(year)
        month = getMonth(month)

    return year, month

def humanMonth(x):
    month = 'Dezembro'

    if x == 1:
        month = 'Janeiro'
    elif x == 2:
        month = 'Fevereiro'
    elif x == 3:
        month = 'Março'
    elif x == 4:
        month = 'Abril'
    elif x == 5:
        month = 'Maio'        
    elif x == 6:
        month = 'Junho'
    elif x == 7:
        month = 'Julho'
    elif x == 8:
        month = 'Agosto'        
    elif x == 9:
        month = 'Setembro'
    elif x == 10:
        month = 'Outubro'
    elif x == 11:
        month = 'Novembro'

    return month

@app.route('/prediction', methods=['GET'])
def predictionSearch():
    slider = request.args.getlist('predicao')

    if len(slider) == 0:
        slider = ['2010']
    year, month = getYearAndMonth(slider[0])
    data = DenguePrediction.get_points(year, month)

    bairros = getBairros()
    points = []
    for value in data:
        try:
            latlon = bairros[str(value[0])]

            if latlon != None:
                points.append([latlon[0], latlon[1], float(value[1])])
        except KeyError:
            pass

    maxYearValue = int(DenguePrediction.get_max_value(year))
    
    return render_template('prediction.html', points=points, sliderValue=slider[0], yearMaxNumber=maxYearValue, year=year, month=humanMonth(month))

@app.route('/setores')
def setores():
    json_url = os.path.join(APP_ROOT, "layers/", "Setores.json")
    data = json.load(open(json_url))
    return jsonify(data)


@app.route('/bairros')
def bairros():
    json_url = os.path.join(APP_ROOT, "layers/", "Bairros.json")
    data = json.load(open(json_url))
    return jsonify(data)


def getSetores():
    result = dict()
    setores = db.session.query(Setores).all()
    for Setor in setores:
        result[Setor.CD_GEOCODI] = [Setor.lat, Setor.lon, Setor.area/1000000.0, Setor.population]
    return result


@app.route('/sifilis/ano', methods=['GET'])
def get_sifilis_por_ano():
    sifilis_adquirida = SifilisAdquirida.get_total_sifilis_ano()
    sifilis_congenita = SifilisCongenita.get_total_sifilis_ano()
    sififilis_gestante = SifilisGestante.get_total_sifilis_ano()
    return dict(Counter(sifilis_adquirida)+Counter(sifilis_congenita)+Counter(sififilis_gestante))


@app.route('/sifilis/ano/sexo', methods=['GET'])
def get_proporcao_sifilis_ano_sexo():
    sa_f, sa_m  = SifilisAdquirida.get_proporcao_sifilis_ano_sexo()
    sc_f, sc_m = SifilisCongenita.get_proporcao_sifilis_ano_sexo()
    data_g = SifilisGestante.get_total_sifilis_ano()

    data_f = dict(Counter(sa_f)+Counter(sc_f))
    data_m = dict(Counter(sa_m)+Counter(sc_m))
    data_fmg = dict(Counter(data_m)+Counter(data_f)+Counter(data_g))

    for i in data_fmg:
      try:
        data_f[i] = round(((data_f[i]/data_fmg[i]) * 100),2);
      except:
        data_f[i] = 0;
      try:
        data_m[i] = round(((data_m[i]/data_fmg[i]) * 100),2);
      except:
        data_m[i] = 0;
      try:
        data_g[i] = round(((data_g[i]/data_fmg[i]) * 100),2);
      except:
        data_g[i] = 0;
    return json.dumps({'F': data_f, 'M' : data_m, 'G' : data_g})


@app.route('/heatmap', methods=['GET'])
def heatmap():
    global HEATMAP_TYPE
    HEATMAP_TYPE = request.args.get('value')
    return "OK"

@app.route('/search', methods=['GET'])
def search():
    global HEATMAP_TYPE
    points = []
    setores = getSetores()

    visualizar = request.args.getlist('visualizar')

    agravos = request.args.getlist('agravo')

    if len(agravos) == 0:
        agravos = ['adquirida', 'congenita', 'gestacional']

    data = {}
    if 'notificacao' in(visualizar):
        if 'adquirida' in agravos:
            data['0'] = SifilisAdquirida.get_filtered_points(request.args)
        if 'congenita' in agravos:
            data['1'] = SifilisCongenita.get_filtered_points(request.args)
        if 'gestacional' in agravos:
            data['2'] = SifilisGestante.get_filtered_points(request.args)
    else:
        if 'obito' in(visualizar):
            data['0'] = Sim.get_filtered_points(request.args)

    for key in data:
        p = []
        for value in data[key]:
            sector_id, num_cases = value
            lat, lon, area, population = setores[str(sector_id)]

            if area != 0 and population != 0:
                density = population / area # calc density for a SC
            else:
                density = DENSITY # use median density

            heat_density = num_cases / density
            heat_area = num_cases / area
            p.append([lat, lon, num_cases, heat_density, heat_area]) # trying to show a better value in the heatmap

        points += p
    return render_template('index.html', points=points, region=get_regions(), heatmap_type=HEATMAP_TYPE)

def get_neighborhoods():
    neighborhoods = SifilisCongenita.get_neighborhoods()
    neighborhoods.update(SifilisAdquirida.get_neighborhoods())
    neighborhoods.update(SifilisGestante.get_neighborhoods())

    dataJson = []

    for key in neighborhoods:
        dataJson.append({"id" : key, "nome" : neighborhoods[key]})

    return dataJson
