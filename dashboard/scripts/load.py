from flask_script import Manager

from shapely.geometry import Polygon

from dashboard import APP_ROOT, app, db
from dashboard.models.SetoresModel import Setores
from dashboard.models.BairrosModel import Bairros

from area import area

import json
import os
import psycopg2
import pandas as pd
import xlrd

manager = Manager(app)

@manager.command
def load_sifilis_congenita():
    #necessario modificar o arquivo de entrada realizando replace de ("[^",]+),([^"]+") por $1|$2 (definir)
    remove_comma_from_csv('sinan_sifilis_congenita_ime.csv', 'tmp_sinan_sifilis_congenita_ime.csv')
    conn = psycopg2.connect(database="dados_sus", user="user_sus", password="pass", host="db", port="5432")
    cur = conn.cursor()
    with open('tmp_sinan_sifilis_congenita_ime.csv', 'r', encoding="latin_1")  as f:
        cur.copy_from(f, 'sifilis_congenita', sep=',', columns=['ID_SINAN_SYPHILIS_CONGENITAL','ID_SINAN_SYPHILIS','id_dataset_sinan','syphilis_diagnosis','dt_notific','nu_ano','dt_diag','nu_idade','female','cs_gestant','id_syphilis_type','dt_sin_pri','ant_dt_nas','ant_idade','cs_sexo','evo_diag_f','evo_diag_n','id_dg_not','dt_obito','dt_encerra','nu_notific','sem_not','id_municip','id_unidade','id_regiona','sg_uf_not','sem_pri','cs_raca','cs_escolar','cs_escol_n','id_ev_not','evolucao','nu_lote','nu_lote_v','nu_lote_h','in_vincula','nduplic','nduplic_n','dt_invest','id_ocupaca','escol_mae','ant_gestac','ant_partos','ant_aborto','ant_natimo','ant_pre_na','ant_porque','ant_local','ant_consul','ant_semana','ant_meses','ant_sifili','ant_forma','ant_parcei','ant_tratad','ant_eviden','lab_vdrl1','lab_titulo','lab_dt1','lab_vdrl2','lab_titu_1','lab_dt2','lab_parto','lab_titu_2','lab_dt3','lab_conf','lab_diag','tra_esquem','tra_dt','ant_uf_cri','ant_muni_c','ant_local_','ant_sema_1','ant_meses_','ant_peso_c','ant_abor_1','ant_nati_1','ant_obito_','ant_dt_obi','ant_exphiv','cli_assint','cli_icteri','cli_rinite','cli_anemia','cli_hepato','cli_esplen','cli_lesoes','cli_osteo','cli_pseudo','cli_outro','cli_desc_o','labc_umbil','labc_titul','labc_igm','labc_sangu','labc_tit_1','labc_dt_1','labc_igg','labc_liquo','labc_tit_2','labc_dt_2','labc_tpha','labc_evide','labc_liq_1','labc_outro','tra_diag_t','tra_esqu_1','tra_dt_tra','evo_diagno','labc_dt_3','labc_hiv','in_aids','tp_not','id_agravo','dt_digita','dt_transus','dt_transdm','dt_transsm','dt_transrm','dt_transrs','dt_transse','cs_flxret','ident_micr','migrado_w','ant_raca','id_ocupa_n','escolmae','uf_pre_nat','mun_pre_na','uni_pre_na','antsifil_n','labc_dt','hepato','lesoes','sin_outr_e','ds_esquema','ds_obs','sem_diag','geo_sifil_id_municipio_6d','geo_sifil_id_municipio_7d','geo_sifil_nm_municipio','geo_sifil_code_tract','geo_sifil_zone','geo_sifil_code_muni','geo_sifil_name_muni','geo_sifil_name_neighborhood','geo_sifil_code_neighborhood','geo_sifil_code_subdistrict','geo_sifil_name_subdistrict','geo_sifil_code_district','geo_sifil_name_district','geo_sifil_code_state'])
        conn.commit()
        return '201'
    return '500'

@manager.command
def load_sifilis_adquirida():
    conn = psycopg2.connect(database="dados_sus", user="user_sus", password="pass", host="db", port="5432")
    cur = conn.cursor()
    with open('sinan_sifilis_adquirida_ime.csv', 'r', encoding="latin_1")  as f:
        cur.copy_from(f, 'sifilis_adquirida', sep=',', columns=['id_sinan_syphilis_acquired','id_sinan_syphilis','id_dataset_sinan','syphilis_diagnosis','dt_notific','nu_ano','dt_diag','nu_idade','female','cs_gestant','id_syphilis_type','dt_sin_pri','cs_sexo','classi_fin','id_dg_not','dt_obito','dt_encerra','nu_notific','sem_not','id_municip','id_unidade','id_regiona','sg_uf_not','sem_pri','cs_raca','cs_escolar','cs_escol_n','id_ev_not','evolucao','nu_lote','nu_lote_v','nu_lote_h','in_vincula','nduplic','nduplic_n','id_agravo','in_confirm','cs_suspeit','in_aids','tp_not','dt_invest','criterio','tpautocto','coufinf','copaisinf','comuninf','codisinf','co_bainfc','nobaiinf','doenca_tra','dt_digita','dt_transus','dt_transdm','dt_transsm','dt_transrm','dt_transrs','dt_transse','cs_flxret','flxrecebi','ident_micr','migrado_w','cs_mening','id_ocupa_n','co_usucad','co_usualt','geo_sifil_id_municipio_6d','geo_sifil_id_municipio_7d','geo_sifil_nm_municipio','geo_sifil_code_tract','geo_sifil_zone','geo_sifil_code_muni','geo_sifil_name_muni','geo_sifil_name_neighborhood','geo_sifil_code_neighborhood','geo_sifil_code_subdistrict','geo_sifil_name_subdistrict','geo_sifil_code_district','geo_sifil_name_district','geo_sifil_code_state'])
        conn.commit()
        return '201'
    return '404'

def setores_population():
    COL_COD_SETOR = 0   # col number of COD_SETOR in base.xls
    COL_POPULATION = 33 # col number of VO14 (total population for a given Setor) in base.xls
    COL_COD_MUNI = 12   # col numver of COD MUNICIPIO in base.xls

    workbook = xlrd.open_workbook(os.path.join(APP_ROOT, "layers/", "base.xls"))
    worksheet = workbook.sheet_by_index(0)
    nrows = worksheet.nrows

    pop_per_sector = dict()

    for i in range(1, nrows):
        if worksheet.cell_value(i, COL_COD_MUNI) == 'RIO DE JANEIRO':
            CD_GEOCODI = str(int(worksheet.cell_value(i, COL_COD_SETOR)))
            POPULATION = int(worksheet.cell_value(i, COL_POPULATION))
            pop_per_sector[CD_GEOCODI] = POPULATION

    return pop_per_sector

@manager.command
def load_setores():
    json_url = os.path.join(APP_ROOT, "layers/", "RJ_complete.json")
    data = json.load(open(json_url))

    count = 0
    area_m2 = 0.0

    pop_per_sector = setores_population()
    for sc in data['features']:
        coords = None
        if sc['geometry']['type'] != 'MultiPolygon':
            pol = Polygon(sc['geometry']['coordinates'][0])
            coords = pol.centroid.coords[0]
        else:
            pol = Polygon(sc['geometry']['coordinates'][0][0])
            coords = pol.centroid.coords[0]
        lon = coords[0]
        lat = coords[1]
        CD_GEOCODI = sc["properties"]["CD_GEOCODI"]
        area_m2 = area(sc['geometry'])

        try:
            POPULATION = pop_per_sector[CD_GEOCODI]
        except KeyError:
            print("Population info not found")
            POPULATION = 0

        sc = Setores(lat, lon, CD_GEOCODI, area_m2, POPULATION)

        db.session.add(sc)
        db.session.commit()
        count += 1
    print("Amount of lines saved: ", count)

@manager.command
def load_bairros():
    json_url = os.path.join(APP_ROOT, "layers/", "Bairros.json")
    data = json.load(open(json_url))

    count = 0
    for b in data['features']:
        coords = None
        if b['geometry']['type'] != 'MultiPolygon':
            pol = Polygon(b['geometry']['coordinates'][0]).centroid
            coords = pol.coords[0]
        else:
            pol = Polygon(b['geometry']['coordinates'][0][0]).centroid
            coords = pol.coords[0]
        lon = coords[0]
        lat = coords[1]
        NM_BAIRRO = b["properties"]["NM_BAIRRO"]

        b = Bairros(lat, lon, NM_BAIRRO)

        db.session.add(b)
        db.session.commit()
        count += 1

    print("Amount of lines saved: ", count)

@manager.command
def load_sifilis_gestante():
    remove_comma_from_csv('sinan_sifilis_gestante_ime.csv', 'tmp_sinan_sifilis_gestante_ime.csv')
    conn = psycopg2.connect(database="dados_sus", user="user_sus", password="pass", host="db", port="5432")
    cur = conn.cursor()
    with open('tmp_sinan_sifilis_gestante_ime.csv', 'r', encoding="latin_1")  as f:
        cur.copy_from(f, 'sifilis_gestante', sep=',', columns=['id_sinan_syphilis_gestational','id_sinan_syphilis','id_syphilis_type','id_dataset_sinan','syphilis_diagnosis','dt_notific','nu_ano','dt_diag','nu_idade_n','female','cs_gestant','cs_sexo','classi_fin','tpconfirma','nu_notific','id_municip','id_unidade','id_regiona','sg_uf_not','cs_raca','cs_escol_n','nu_lote_v','nu_lote_h','nduplic_n','tp_not','id_agravo','dt_digita','dt_transus','dt_transdm','dt_transsm','dt_transrm','dt_transrs','dt_transse','cs_flxret','ident_micr','migrado_w','id_ocupa_n','pre_ufrel','pre_munire','pre_unipre','pre_sispre','tpevidenci','tpteste1','dstitulo1','dtteste1','tpesquema','tratparc','tpesqpar','tpmotparc','dsmotivo','ds_obs','sem_diag','geo_sifil_id_municipio_6d','geo_sifil_id_municipio_7d','geo_sifil_nm_municipio','geo_sifil_code_tract','geo_sifil_zone','geo_sifil_code_muni','geo_sifil_name_muni','geo_sifil_name_neighborhood','geo_sifil_code_neighborhood','geo_sifil_code_subdistrict','geo_sifil_name_subdistrict','geo_sifil_code_district','geo_sifil_name_district','geo_sifil_code_state'])
        conn.commit()
        return '201'
    return '500'

def remove_comma_from_csv(path, file_name):
    df_read = pd.read_csv(path, sep = ',' , keep_default_na=False, dtype=str, quotechar='"', engine='c', low_memory=False)
    df_read = df_read.applymap(str)
    for column in df_read:
        df_read[column] = [x.replace(',', ' ') for x in df_read[column]]
    df_read.to_csv(file_name, header=False, index = None)

@manager.command
def load_sim():
    remove_comma_from_csv('sim_ime.csv', 'tmp_sim_ime.csv')
    conn = psycopg2.connect(database="dados_sus", user="user_sus", password="pass", host="db", port="5432")
    cur = conn.cursor()
    with open('tmp_sim_ime.csv', 'r', encoding="latin_1")  as f:
        cur.copy_from(f, 'sim', sep=',', columns=['id_sim','id_dataset_sim','ano','mes','horaobito','fetal_death','infant_death','v_natural','codmunnatu','idade','sexo','female','racacor','estciv','ocup','esc','esc2010','seriescfal','escfalagr1','escfalagr2','cnes','lococor','codestocor','codmunocor','baiocor','codbaiocor','endocor','codendocor','codregocor','numendocor','complocor','cepocor','m_causamat','m_tpmorteoco','m_obitograv','m_obitopuerp','cid_causabas','capcid_causabas','gpcid_causabas','icd10','linhaa','linhab','linhac','linhad','linhaii','cid_causaori','capcid_causaori','gpcid_causaori','linhaa_o','linhab_o','linhac_o','linhad_o','linhaii_o','cid_causabas_regra','assistmed','diag_exame','diag_cirurgia','diag_necropsia','f_idademae','f_escmae','f_escmae2010','f_seriescmae','f_escmaeagr1','f_escmaeagr2','f_ocupmae','f_qtdfilvivo','f_qtdfilmort','f_gravidez','f_gestacao','f_partonormal','f_obitoparto','f_peso','f_nsemanagest','ext_circobito','ext_acidtrab','ext_fonte','invest_obit','invest_dt','invest_fonte','med_atestante','med_dtatestado','med_comunsvoim','acid_descr','acid_end','acid_codend','acid_numend','acid_complend','acid_cep','cart_codest','cart_codmun','cart_cod','cart_numreg','cart_dtreg','r_cid_causabas','r_cid_caub','r_icd10','r_icd10b','r_status','r_explic','r_compara_cb','r_nonres','r_nonresc','r_dtressele','adm_retroalim','adm_dtcadastro','adm_numerolote','geo_obit_id_municipio_6d','geo_obit_id_municipio_7d','geo_obit_nm_municipio','geo_obit_code_tract','geo_obit_zone','geo_obit_code_muni','geo_obit_name_muni','geo_obit_name_neighborhood','geo_obit_code_neighborhood','geo_obit_code_subdistrict','geo_obit_name_subdistrict','geo_obit_code_district','geo_obit_name_district','geo_obit_code_state'])
        conn.commit()

@manager.command
def load_dengue_prediction():
    conn = psycopg2.connect(database="dados_sus", user="user_sus", password="pass", host="db", port="5432")
    cur = conn.cursor()
    with open('previsao_dengue_2010_2020.csv', 'r', encoding="latin_1")  as f:
        cur.copy_from(f, 'dengue_prediction', sep=',', columns=['n','code_neighborhood','name_neighborhood','ano','mes','dengue_diagnosis'])
        conn.commit()
        return '201'
    return '500'