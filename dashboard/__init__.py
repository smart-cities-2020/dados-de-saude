from flask import Flask
from flask_cors import CORS, cross_origin
from flask_sqlalchemy import SQLAlchemy

import os

app = Flask(__name__)

APP_ROOT = os.path.realpath(os.path.dirname(__file__))

cors = CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

from dashboard.controllers import route