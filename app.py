import subprocess

from dashboard import app, db

if __name__ == '__main__':
    subprocess.run(["python", "manage.py", "db", "init"])
    subprocess.run(["python", "manage.py", "db", "migrate"])
    subprocess.run(["python", "manage.py", "db", "upgrade"])

    # subprocess.run(["python", "manage.py", "load_setores_data"])
    # subprocess.run(["python", "manage.py", "load_adquirida_data"])

    app.run(debug=True, host='0.0.0.0')
