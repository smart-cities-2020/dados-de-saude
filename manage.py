import os
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import app
from dashboard import db

from dashboard.models.SetoresModel import Setores
from dashboard.models.BairrosModel import Bairros
from dashboard.models.SifilisAdquiridaModel import SifilisAdquirida
from dashboard.models.SifilisCongenitaModel import SifilisCongenita
from dashboard.models.SifilisGestanteModel import SifilisGestante
from dashboard.models.SimModel import Sim
from dashboard.models.DenguePredictionModel import DenguePrediction

from dashboard.scripts import load

app.config.from_object(os.environ['APP_SETTINGS'])

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

@manager.command
def load_setores_data():
    load.load_setores()

@manager.command
def load_bairros_data():
    load.load_bairros()

@manager.command
def load_adquirida_data():
    load.load_sifilis_adquirida()

@manager.command
def load_congenita_data():
    load.load_sifilis_congenita()

@manager.command
def load_gestante_data():
    load.load_sifilis_gestante()

@manager.command
def load_sim_data():
    load.load_sim()

@manager.command
def load_dengue_prediction_data():
    load.load_dengue_prediction()

# manager.add_command('load_setores_data', load_setores_data())
# manager.add_command('load_adquirida_data', load_adquirida_data())
# manager.add_command('load_congenita_data', load_congenita_data())

if __name__ == '__main__':
    manager.run()