#!/bin/bash
set -e

psql -U postgres -d template1 -c 'create role user_sus with login createdb;'
psql -U postgres -d template1 -c "ALTER role user_sus WITH PASSWORD 'pass';"
psql -U postgres -d template1 -c 'create database dados_sus owner user_sus;'