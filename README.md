# saude

Sistema de visualização e análise de dados de saúde pública. O projeto será feito em parceria com o IEPS.

# up

    docker-compose build
    docker-compose up -d

    docker-compose exec dashboard python manage.py load_setores_data
    docker-compose exec dashboard python manage.py load_bairros_data
    docker-compose exec dashboard python manage.py load_adquirida_data
    docker-compose exec dashboard python manage.py load_congenita_data
    docker-compose exec dashboard python manage.py load_gestante_data
    docker-compose exec dashboard python manage.py load_sim_data
    docker-compose exec dashboard python manage.py load_dengue_prediction_data
